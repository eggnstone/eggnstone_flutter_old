import 'package:eggnstone_flutter/tools/Date.dart';
import 'package:eggnstone_flutter/tools/DateAndTime.dart';
import 'package:eggnstone_flutter/tools/Time.dart';

abstract class ISharedPreferencesService
{
    bool getBool(String key, bool def);

    void setBool(String key, bool value);

    Date getDate(String key, Date def);

    void setDate(String key, Date value);

    Time getTime(String key, Time def);

    void setTime(String key, Time value);

    DateAndTime getDateAndTime(String key, DateAndTime def);

    void setDateAndTime(String key, DateAndTime value);

    DateTime getDateTime(String key, DateTime def);

    void setDateTime(String key, DateTime value);

    int getInt(String key, int def);

    void setInt(String key, int value);

    List<int> getIntList(String key, List<int> def);

    void setIntList(String key, List<int> intList, {bool removeIfEmpty = true});

    String getString(String key, String def);

    void setString(String key, String value);

    List<String> getStringList(String key, List<String> def);

    void setStringList(String key, List<String> stringList, {bool removeIfEmpty = true});
}
