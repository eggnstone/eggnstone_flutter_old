import 'dart:async';

import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';
import 'package:eggnstone_flutter/services/shared_preferences/ISharedPreferencesService.dart';
import 'package:eggnstone_flutter/tools/Date.dart';
import 'package:eggnstone_flutter/tools/DateAndTime.dart';
import 'package:eggnstone_flutter/tools/Time.dart';
import 'package:shared_preferences/shared_preferences.dart' as sp;

/// Requires [ILoggerService]
class SharedPreferencesService
    with LoggerMixin
    implements ISharedPreferencesService
{
    sp.SharedPreferences _sharedPreferences;

    SharedPreferencesService._internal()
    {
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [ILoggerService]
    static Future<SharedPreferencesService> create()
    async
    {
        var sharedPreferences = SharedPreferencesService._internal();
        await sharedPreferences._init();
        return Future.value(sharedPreferences);
    }

    Future<void> _init()
    async
    {
        _sharedPreferences = await sp.SharedPreferences.getInstance();
    }

    @override
    bool getBool(String key, bool def)
    {
        try
        {
            return _sharedPreferences?.getBool(key) ?? def;
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setBool(String key, bool value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setBool(key, value);
    }

    @override
    Date getDate(String key, Date def)
    {
        try
        {
            String s = _sharedPreferences?.getString(key);
            if (s == null)
                return def;

            return Date.parse(s);
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setDate(String key, Date value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setString(key, value.toString());
    }

    @override
    Time getTime(String key, Time def)
    {
        try
        {
            String s = _sharedPreferences?.getString(key);
            if (s == null)
                return def;

            return Time.parse(s);
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setTime(String key, Time value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setString(key, value.toString());
    }

    @override
    DateAndTime getDateAndTime(String key, DateAndTime def)
    {
        try
        {
            String s = _sharedPreferences?.getString(key);
            if (s == null)
                return def;

            return DateAndTime.parse(s);
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setDateAndTime(String key, DateAndTime value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setString(key, value.toString());
    }

    @override
    DateTime getDateTime(String key, DateTime def)
    {
        try
        {
            String s = _sharedPreferences?.getString(key);
            if (s == null)
                return def;

            return DateTime.parse(s);
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setDateTime(String key, DateTime value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setString(key, value.toIso8601String());
    }

    @override
    int getInt(String key, int def)
    {
        try
        {
            return _sharedPreferences?.getInt(key) ?? def;
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setInt(String key, int value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setInt(key, value);
    }

    @override
    List<int> getIntList(String key, List<int> def)
    {
        try
        {
            List<String> stringList = _sharedPreferences?.getStringList(key);
            if (stringList == null)
                return def;

            var intList = List<int>();
            for (String s in stringList)
                intList.add(int.parse(s));

            return intList;
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setIntList(String key, List<int> intList, {bool removeIfEmpty = true})
    {
        if (_sharedPreferences == null)
            return;

        if (intList == null)
            _sharedPreferences.remove(key);
        else if (intList.length == 0 && removeIfEmpty)
            _sharedPreferences.remove(key);
        else
        {
            var stringList = List<String>();

            for (int i in intList)
                stringList.add(i.toString());

            _sharedPreferences.setStringList(key, stringList);
        }
    }

    @override
    String getString(String key, String def)
    {
        try
        {
            return _sharedPreferences?.getString(key) ?? def;
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setString(String key, String value)
    {
        if (_sharedPreferences == null)
            return;

        if (value == null)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setString(key, value);
    }

    @override
    List<String> getStringList(String key, List<String> def)
    {
        try
        {
            return _sharedPreferences?.getStringList(key) ?? def;
        }
        catch (e)
        {
            logger.logInfo(e.toString());
            logger.logInfo(_sharedPreferences?.get(key));
            return def;
        }
    }

    @override
    void setStringList(String key, List<String> stringList, {bool removeIfEmpty = true})
    {
        if (_sharedPreferences == null)
            return;

        if (stringList == null)
            _sharedPreferences.remove(key);
        else if (stringList.length == 0 && removeIfEmpty)
            _sharedPreferences.remove(key);
        else
            _sharedPreferences.setStringList(key, stringList);
    }
}
