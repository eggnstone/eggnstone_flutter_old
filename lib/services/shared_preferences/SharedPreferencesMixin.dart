import 'package:eggnstone_flutter/services/shared_preferences/ISharedPreferencesService.dart';
import 'package:get_it/get_it.dart';

mixin SharedPreferencesMixin
{
    ISharedPreferencesService get sharedPreferences
    => GetIt.instance.get<ISharedPreferencesService>();
}
