import 'package:firebase_analytics/observer.dart';

abstract class IAnalyticsService
{
    static const String EVENT_NAME__TUTORIAL_BEGIN = 'tutorial_begin';
    static const String EVENT_NAME__TUTORIAL_COMPLETE = 'tutorial_complete';
    static const int MAX_EVENT_NAME_LENGTH = 40;
    static const int MAX_PARAM_NAME_LENGTH = 40;
    static const int MAX_PARAM_VALUE_LENGTH = 100;

    FirebaseAnalyticsObserver getObserver();

    void setEnabled(bool enabled);

    String getCurrentScreen();

    void setCurrentScreen(String name);

    void log(String name, {String action, Object value});

    void logGeneric(String name, Map<String, dynamic> params);

    void setUserProperty(String name, String value, {bool force = false});

    void setUserId(String value);
}

/*
    String text40 = 'Z_Test_40_chars_678901234567890123456789';
    String text41 = 'Z_Test_41_chars_6789012345678901234567890';
    String text100 = 'Z_Test_100_chars_78901234567890123456789012345678901234567890123456789012345678901234567890123456789';
    String text101 = 'Z_Test_101_chars_789012345678901234567890123456789012345678901234567890123456789012345678901234567891';

    Globals.analytics.log(text40);
    Globals.analytics.log(text41);

    Globals.analytics.log(text40, action: text100);
    Globals.analytics.log(text40, action: text101);

    Globals.analytics.log(text40, value: text100);
    Globals.analytics.log(text40, value: text101);

    Globals.analytics.logGeneric(text40, {text40: text100}); // all ok
    Globals.analytics.logGeneric(text41, {text40: text100});
    Globals.analytics.logGeneric(text40, {text41: text100});
    Globals.analytics.logGeneric(text40, {text40: text101});
    Globals.analytics.logGeneric(text41, {text41: text101}); // all too long
*/
