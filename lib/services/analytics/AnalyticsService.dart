import 'package:eggnstone_flutter/services/analytics/IAnalyticsService.dart';
import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';
import 'package:eggnstone_flutter/tools/Observable.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

/// Requires [ILoggerService]
class AnalyticsService
    with LoggerMixin
    implements IAnalyticsService
{
    final Observable<bool> _isDebugModeEnabled;

    FirebaseAnalytics _firebaseAnalytics;
    FirebaseAnalyticsObserver _firebaseAnalyticsObserver;
    bool _isEnabled;
    String _currentScreen;

    AnalyticsService._internal(this._isEnabled, this._isDebugModeEnabled)
    {
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [ILoggerService]
    static Future<AnalyticsService> create(bool isEnabled, Observable<bool> isDebugModeEnabled)
    async
    {
        var analyticsService = AnalyticsService._internal(isEnabled, isDebugModeEnabled);
        await analyticsService._init();
        return analyticsService;
    }

    void _init()
    async
    {
        _firebaseAnalytics = FirebaseAnalytics();
        _firebaseAnalyticsObserver = FirebaseAnalyticsObserver(analytics: _firebaseAnalytics);
        await _firebaseAnalytics.setAnalyticsCollectionEnabled(_isEnabled);
    }

    @override
    FirebaseAnalyticsObserver getObserver()
    {
        return _firebaseAnalyticsObserver;
    }

    @override
    void setEnabled(bool enabled)
    {
        logger.logInfo('Analytics.setEnabled($enabled)');
        _firebaseAnalytics.setAnalyticsCollectionEnabled(enabled);
        _isEnabled = enabled;
    }

    @override
    String getCurrentScreen()
    => _currentScreen;

    @override
    void setCurrentScreen(String name)
    {
        if (_isDebugModeEnabled.value)
        {
            String s = _isEnabled ? 'Analytics: ' : 'Dummy-Analytics: ';
            logger.logInfo(s + 'setCurrentScreen: ' + name);
        }

        if (_isEnabled)
            _firebaseAnalytics.setCurrentScreen(screenName: name, screenClassOverride: name);

        _currentScreen = name;
    }

    @override
    void log(String name, {String action, Object value})
    {
        Map<String, dynamic> params = {};

        if (action != null)
            params['Action'] = action;

        if (value != null)
            params['Value'] = value;

        logGeneric(name, params);
    }

    @override
    void logGeneric(String name, Map<String, dynamic> params)
    {
        // remove null params as they prevent the logging
        List<String> keys = params.keys.toList(growable: false);
        for (int i = 0; i < keys.length; i++)
        {
            String key = keys[i];
            if (params[key] == null)
            {
                params.remove(key);
                logger.logInfo('Analytics: Removed key with null value: $key');
            }
        }

        if (_isDebugModeEnabled.value)
        {
            String s = (_isEnabled ? 'Analytics: ' : 'Dummy-Analytics: ') + name;

            for (var key in params.keys)
                s += ' $key=${params[key]}';

            logger.logInfo(s);
        }

        // Check limits (https://support.google.com/firebase/answer/9237506?hl=en)
        bool foundError = false;

        if (name.length > IAnalyticsService.MAX_EVENT_NAME_LENGTH)
        {
            foundError = true;
            logger.logError('##################################################');
            logger.logError('# Error: Event name "$name" is too long! Is=${name.length} Max=${IAnalyticsService.MAX_EVENT_NAME_LENGTH}');

            if (_isEnabled)
                _firebaseAnalytics.logEvent(
                    name: 'AnalyticsError',
                    parameters: {
                        'EventName': name.length <= IAnalyticsService.MAX_PARAM_VALUE_LENGTH ? name : name.substring(0, IAnalyticsService.MAX_PARAM_VALUE_LENGTH),
                        'Message': 'Event name is too long! Is=${name.length} Max=${IAnalyticsService.MAX_EVENT_NAME_LENGTH}'
                    }
                );
        }

        for (var key in params.keys)
        {
            if (key.length > IAnalyticsService.MAX_PARAM_NAME_LENGTH)
            {
                foundError = true;
                logger.logError('##################################################');
                logger.logError('# Error: Param name "$key" is too long! Is=${key.length} Max=${IAnalyticsService.MAX_PARAM_NAME_LENGTH}');

                if (_isEnabled)
                    _firebaseAnalytics.logEvent(
                        name: 'AnalyticsError',
                        parameters: {
                            'EventName': name.length <= IAnalyticsService.MAX_PARAM_VALUE_LENGTH ? name : name.substring(0, IAnalyticsService.MAX_PARAM_VALUE_LENGTH),
                            'ParamName': key.length <= IAnalyticsService.MAX_PARAM_VALUE_LENGTH ? key : key.substring(0, IAnalyticsService.MAX_PARAM_VALUE_LENGTH),
                            'Message': 'Param name is too long! Is=${key.length} Max=${IAnalyticsService.MAX_PARAM_NAME_LENGTH}'
                        }
                    );
            }

            var value = params[key];
            if (value is String && value.length > IAnalyticsService.MAX_PARAM_VALUE_LENGTH)
            {
                foundError = true;
                logger.logError('##################################################');
                logger.logError('# Error: Param value of "$key" is too long! Is=${value.length} Max=${IAnalyticsService.MAX_PARAM_VALUE_LENGTH} Value: $value');

                if (_isEnabled)
                    _firebaseAnalytics.logEvent(
                        name: 'AnalyticsError',
                        parameters: {
                            'EventName': name.length <= IAnalyticsService.MAX_PARAM_VALUE_LENGTH ? name : name.substring(0, IAnalyticsService.MAX_PARAM_VALUE_LENGTH),
                            'ParamName': key.length <= IAnalyticsService.MAX_PARAM_VALUE_LENGTH ? key : key.substring(0, IAnalyticsService.MAX_PARAM_VALUE_LENGTH),
                            'ParamValue': value.substring(0, IAnalyticsService.MAX_PARAM_VALUE_LENGTH),
                            'Message': 'Param value is too long! Is=${value.length} Max=${IAnalyticsService.MAX_PARAM_VALUE_LENGTH}'
                        }
                    );
            }
        }

        if (_isEnabled && foundError == false)
            _firebaseAnalytics.logEvent(name: name, parameters: params);
    }

    @override
    void setUserProperty(String name, String value, {bool force = false})
    {
        if (_isEnabled || force)
            _firebaseAnalytics.setUserProperty(name: name, value: value);

        if (_isDebugModeEnabled.value)
        {
            String s = (_isEnabled ? 'Analytics: ' : 'Dummy-Analytics: ') + name;
            logger.logInfo(s + 'setUserProperty: name=$name value=$value force=$force');
        }
    }

    @override
    void setUserId(String value)
    {
        if (_isEnabled)
            _firebaseAnalytics.setUserId(value);

        if (_isDebugModeEnabled.value)
        {
            String s = _isEnabled ? 'Analytics: ' : 'Dummy-Analytics: ';
            logger.logInfo(s + 'setUserId: $value');
        }
    }
}

/*
    String text40 = 'Z_Test_40_chars_678901234567890123456789';
    String text41 = 'Z_Test_41_chars_6789012345678901234567890';
    String text100 = 'Z_Test_100_chars_78901234567890123456789012345678901234567890123456789012345678901234567890123456789';
    String text101 = 'Z_Test_101_chars_789012345678901234567890123456789012345678901234567890123456789012345678901234567891';

    Globals.analytics.log(text40);
    Globals.analytics.log(text41);

    Globals.analytics.log(text40, action: text100);
    Globals.analytics.log(text40, action: text101);

    Globals.analytics.log(text40, value: text100);
    Globals.analytics.log(text40, value: text101);

    Globals.analytics.logGeneric(text40, {text40: text100}); // all ok
    Globals.analytics.logGeneric(text41, {text40: text100});
    Globals.analytics.logGeneric(text40, {text41: text100});
    Globals.analytics.logGeneric(text40, {text40: text101});
    Globals.analytics.logGeneric(text41, {text41: text101}); // all too long
*/
