import 'package:flutter/widgets.dart';

abstract class ICrashlyticsService
{
    void setEnabled(bool enabled);

    void run(Widget app);

    void reportNonFatalCrash(dynamic e, StackTrace stackTrace);
}
