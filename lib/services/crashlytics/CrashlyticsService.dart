import 'dart:async';

import 'package:eggnstone_flutter/services/crashlytics/ICrashlyticsService.dart';
import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';
import 'package:eggnstone_flutter/tools/Observable.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/widgets.dart';

/// Requires [ILoggerService]
class CrashlyticsService
    with LoggerMixin
    implements ICrashlyticsService
{
    final Observable<bool> _isDebugModeEnabled;

    FirebaseCrashlytics _firebaseCrashlytics;
    bool _isEnabled;

    CrashlyticsService._internal(this._isEnabled, this._isDebugModeEnabled)
    {
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [ILoggerService]
    static Future<CrashlyticsService> create(bool isEnabled, Observable<bool> isDebugModeEnabled)
    async
    {
        var crashlyticsService = CrashlyticsService._internal(isEnabled, isDebugModeEnabled);
        await crashlyticsService._init();
        return crashlyticsService;
    }

    Future<void> _init()
    async
    {
        _firebaseCrashlytics = FirebaseCrashlytics.instance;
        await _firebaseCrashlytics.setCrashlyticsCollectionEnabled(_isEnabled);

        FlutterError.onError = (FlutterErrorDetails details)
        {
            logger.logInfo('FlutterError.onError');
            if (_isDebugModeEnabled.value)
            {
                // In development mode simply print to console.
                logger.logError('##################################################');
                logger.logError(_isEnabled ? 'Crashlytics: ' : 'Dummy-Crashlytics: ');

                FlutterError.dumpErrorToConsole(details);

                if (details.stack == null)
                    logger.logInfo('No stacktrace available.');
                else
                    logger.logInfo(details.stack.toString());

                logger.logError('##################################################');
            }
            else
            {
                // In production mode report to the application zone to report to Crashlytics.
                if (_isEnabled)
                    Zone.current.handleUncaughtError(details.exception, details.stack);
            }
        };
    }

    @override
    void setEnabled(bool enabled)
    {
        logger.logInfo('Crashlytics.setEnabled($enabled)');
        _firebaseCrashlytics.setCrashlyticsCollectionEnabled(enabled);
        _isEnabled = enabled;
    }

    @override
    void run(Widget app)
    {
        if (_isEnabled)
            runZonedGuarded<Future<void>>(
                    ()
                async
                {
                    runApp(app);
                },
                    (Object error, StackTrace stackTrace)
                async
                {
                    // Whenever an error occurs, call the `reportCrash` function. This will send
                    // Dart errors to our dev console or Crashlytics depending on the environment.
                    if (_isDebugModeEnabled.value)
                    {
                        logger.logError('##################################################');
                        logger.logError(error.toString());
                        logger.logError(stackTrace.toString());
                        logger.logError('##################################################');
                    }
                    else
                        await _firebaseCrashlytics.recordError(error, stackTrace);
                });
        else
            runApp(app);
    }

    @override
    void reportNonFatalCrash(dynamic e, StackTrace stackTrace)
    {
        if (_isEnabled)
        {
            _firebaseCrashlytics.log('Crashlytics.reportNonFatalCrash: ' + e.toString());
            _firebaseCrashlytics.recordError(e, stackTrace);
        }
    }
}
