import 'package:eggnstone_flutter/services/crashlytics/ICrashlyticsService.dart';
import 'package:get_it/get_it.dart';

mixin CrashlyticsMixin
{
    ICrashlyticsService get crashlytics
    => GetIt.instance.get<ICrashlyticsService>();
}
