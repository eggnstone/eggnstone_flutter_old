import 'package:eggnstone_flutter/services/logger/ILoggerService.dart';
import 'package:eggnstone_flutter/tools/Time.dart';

class LoggerService
    implements ILoggerService
{
    @override
    bool isEnabled = true;

    LoggerService._internal(this.isEnabled);

    static LoggerService create(bool isEnabled)
    {
        return LoggerService._internal(isEnabled);
    }

    @override
    void logDebug(String message)
    {
        _log('Debug: ' + message);
    }

    @override
    void logInfo(String message)
    {
        _log('Info:  ' + message);
    }

    @override
    void logWarning(String message)
    {
        _log('Warn:  ' + message);
    }

    @override
    void logError(String message)
    {
        _log('ERROR: ' + message);
    }

    void _log(String message)
    {
        if (isEnabled)
            print(Time.now().to24HoursMinutesSecondsString() + ' ' + message);
    }
}
