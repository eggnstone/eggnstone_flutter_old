abstract class ILoggerService
{
    bool isEnabled = true;

    void logDebug(String message);

    void logInfo(String message);

    void logWarning(String message);

    void logError(String message);
}
