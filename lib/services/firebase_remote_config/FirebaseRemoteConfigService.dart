import 'package:eggnstone_flutter/services/firebase_remote_config/IFirebaseRemoteConfigService.dart';
import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

/// Requires [ILoggerService]
class FirebaseRemoteConfigService
    with LoggerMixin
    implements IFirebaseRemoteConfigService
{
    RemoteConfig _remoteConfig;
    bool _initialized = false;

    FirebaseRemoteConfigService._internal()
    {
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [ILoggerService]
    static Future<FirebaseRemoteConfigService> create()
    async
    {
        var instance = FirebaseRemoteConfigService._internal();
        await instance._init();
        return Future.value(instance);
    }

    Future<void> _init()
    async
    {
        try
        {
            _remoteConfig = await RemoteConfig.instance;
            await _remoteConfig.fetch(expiration: const Duration(hours: 24));
            await _remoteConfig.activateFetched();
            _initialized = true;
        }
        catch (exception)
        {
            logger.logInfo('FirebaseRemoteConfig._init: $exception');
        }
    }

    @override
    bool getBool(String key, bool def)
    {
        if (_initialized == false)
        {
            logger.logInfo('FirebaseRemoteConfig.getBool($key): not initialized!');
            return def;
        }

        bool value = _remoteConfig.getBool(key);
        logger.logInfo('FirebaseRemoteConfig.getBool($key): $value');

        return value;
    }
}
