import 'package:eggnstone_flutter/services/firebase_remote_config/IFirebaseRemoteConfigService.dart';
import 'package:get_it/get_it.dart';

mixin FirebaseRemoteConfigMixin
{
    IFirebaseRemoteConfigService get firebaseRemoteConfig
    => GetIt.instance.get<IFirebaseRemoteConfigService>();
}
