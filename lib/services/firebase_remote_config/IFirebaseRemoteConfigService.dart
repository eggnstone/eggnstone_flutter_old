abstract class IFirebaseRemoteConfigService
{
    bool getBool(String key, bool def);
}
