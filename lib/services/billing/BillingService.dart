import 'dart:async';
import 'dart:io';

import 'package:eggnstone_flutter/services/analytics/AnalyticsMixin.dart';
import 'package:eggnstone_flutter/services/billing/IBillingService.dart';
import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';
import 'package:flutter_billing/flutter_billing.dart' as b;

/// Requires [IAnalyticsService] and [ILoggerService]
class BillingService
    with AnalyticsMixin, LoggerMixin
    implements IBillingService
{
    final List<String> _appleProductIds;
    final List<String> _googleProductIds;

    b.Billing _billing;

    BillingService._internal(this._appleProductIds, this._googleProductIds)
    {
        assert(analytics != null, 'Unable to find via GetIt: Analytics');
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [IAnalyticsService] and [ILoggerService]
    static Future<BillingService> create(List<String> appleProductIds, List<String> googleProductIds)
    async
    {
        var b = BillingService._internal(appleProductIds, googleProductIds);
        b._init();
        return Future.value(b);
    }

    Future<void> _init()
    async
    {
        _billing = b.Billing(onError: (dynamic e)
        {
            logger.logInfo('Error in Billing: $e');
        });
    }

    @override
    Future<bool> restorePurchase()
    async
    {
        bool isPremiumPurchased = false;

        analytics.log(IBillingService.ANALYTICS_LOG_KEY_PREMIUM, action: 'Restore', value: 'Started');
        Set<String> purchases = await _getPurchases();

        if (Platform.isIOS)
        {
            for (var productId in _appleProductIds)
                if (purchases.contains(productId))
                    isPremiumPurchased = true;
        }
        else
        {
            for (var productId in _googleProductIds)
                if (purchases.contains(productId))
                    isPremiumPurchased = true;
        }

        if (isPremiumPurchased)
        {
            analytics.log(IBillingService.ANALYTICS_LOG_KEY_PREMIUM, action: 'Restore', value: 'Finished');
        }
        else
        {
            analytics.log(IBillingService.ANALYTICS_LOG_KEY_PREMIUM, action: 'Restore', value: 'Failed');
        }

        return Future.value(isPremiumPurchased);
    }

    @override
    Future<b.BillingProduct> getProduct(String id)
    {
        return Future.value(_billing.getProduct(id));
    }

    @override
    Future<b.BillingProduct> getSubscription(String id)
    {
        return Future.value(_getSubscription(id));
    }

    @override
    Future<bool> purchase(b.BillingProduct product)
    async
    {
        if (product == null)
            return false;

        logger.logInfo('Trying to purchase: $product');
        logger.logInfo('identifier: ${product.identifier}');

        if (await _billing.purchase(product.identifier))
            return true;

        return false;
    }

    @override
    Future<bool> subscribe(b.BillingProduct product)
    async
    {
        if (product == null)
            return false;

        logger.logInfo('Trying to subscribe: $product');
        logger.logInfo('identifier: ${product.identifier}');

        if (await _billing.subscribe(product.identifier))
            return true;

        return false;
    }

    Future<Set<String>> _getPurchases()
    async
    {
        final Set<String> purchases = await _billing.getPurchases();

        if (purchases.length == 0)
        {
            logger.logInfo('Billing.getPurchases: none');
        }
        else
        {
            logger.logInfo('Billing.getPurchases: ' + purchases.length.toString());
            purchases.forEach((p)
            {
                logger.logInfo('Purchase: $p');
            });
        }

        return Future.value(purchases);
    }

    Future<b.BillingProduct> _getSubscription(String identifier)
    async
    {
        final products = await _billing.getSubscriptions(<String>[identifier]);
        return products.firstWhere((product)
        => product.identifier == identifier, orElse: ()
        => null);
    }
}
