import 'dart:async';

import 'package:flutter_billing/flutter_billing.dart' as b;

abstract class IBillingService
{
    static const String ANALYTICS_LOG_KEY_PREMIUM = 'Premium';

    Future<bool> restorePurchase();

    Future<b.BillingProduct> getProduct(String id);

    Future<b.BillingProduct> getSubscription(String id);

    Future<bool> purchase(b.BillingProduct product);

    Future<bool> subscribe(b.BillingProduct product);
}
