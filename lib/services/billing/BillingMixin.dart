import 'package:eggnstone_flutter/services/billing/IBillingService.dart';
import 'package:get_it/get_it.dart';

mixin BillingMixin
{
    IBillingService get billing
    => GetIt.instance.get<IBillingService>();
}
