import 'package:eggnstone_flutter/services/vibrator/IVibratorService.dart';
import 'package:get_it/get_it.dart';

mixin VibratorMixin
{
    IVibratorService get vibrator
    => GetIt.instance.get<IVibratorService>();
}
