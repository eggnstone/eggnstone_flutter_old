import 'dart:async';

import 'package:vibrate/vibrate.dart';

/*
In order of increasing vibration length (Android):

FeedbackType.selection
FeedbackType.impact

FeedbackType.light

FeedbackType.medium
FeedbackType.success

FeedbackType.heavy

FeedbackType.warning

FeedbackType.error
*/

abstract class IVibratorService
{
    Future<void> vibrate(FeedbackType type);
}
