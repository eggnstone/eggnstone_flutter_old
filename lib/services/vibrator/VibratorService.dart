import 'dart:async';

import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';
import 'package:eggnstone_flutter/services/vibrator/IVibratorService.dart';
import 'package:vibrate/vibrate.dart';

/*
In order of increasing vibration length (Android):

FeedbackType.selection
FeedbackType.impact

FeedbackType.light

FeedbackType.medium
FeedbackType.success

FeedbackType.heavy

FeedbackType.warning

FeedbackType.error
*/

/// Requires [ILoggerService]
class VibratorService
    with LoggerMixin
    implements IVibratorService
{
    String type;
    String info;

    VibratorService._internal()
    {
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [ILoggerService]
    static Future<VibratorService> create()
    async
    {
        var instance = VibratorService._internal();
        return Future.value(instance);
    }

    @override
    Future<void> vibrate(FeedbackType type)
    async
    {
        final bool canVibrate = await Vibrate.canVibrate;
        if (canVibrate)
            Vibrate.feedback(type);
        else
            logger.logInfo('CannotVibrate: ' + type.toString());
    }
}
