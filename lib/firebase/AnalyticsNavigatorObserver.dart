import 'package:eggnstone_flutter/services/analytics/IAnalyticsService.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';

typedef String ScreenNameExtractor(RouteSettings settings);

String defaultNameExtractor(RouteSettings settings)
=> settings.name;

class AnalyticsNavigatorObserver extends NavigatorObserver
{
    AnalyticsNavigatorObserver();

    final ScreenNameExtractor nameExtractor = defaultNameExtractor;

    @override
    void didPush(Route<dynamic> route, Route<dynamic> previousRoute)
    {
        super.didPush(route, previousRoute);

        /*if (previousRoute != null)
        {
            print("didPush: previousRoute:             ${nameExtractor(previousRoute.settings)}");
            print("didPush: previousRoute.runtimeType: ${previousRoute.runtimeType}");
            print("didPush: previousRoute.isPageRoute: ${previousRoute is PageRoute}");
        }

        if (route != null)
        {
            print("didPush: route:             ${nameExtractor(route.settings)}");
            print("didPush: route.runtimeType: ${route.runtimeType}");
            print("didPush: route.isPageRoute: ${route is PageRoute}");
        }*/

        if (route is PageRoute)
            _sendScreenView(route);
    }

    @override
    void didReplace({Route<dynamic> newRoute, Route<dynamic> oldRoute})
    {
        super.didReplace(newRoute: newRoute, oldRoute: oldRoute);

        /*if (oldRoute != null)
        {
            print("didReplace: oldRoute:             ${nameExtractor(oldRoute.settings)}");
            print("didReplace: oldRoute.runtimeType: ${oldRoute.runtimeType}");
            print("didReplace: oldRoute.isPageRoute: ${oldRoute is PageRoute}");
        }

        if (newRoute != null)
        {
            print("didReplace: newRoute:             ${nameExtractor(newRoute.settings)}");
            print("didReplace: newRoute.runtimeType: ${newRoute.runtimeType}");
            print("didReplace: newRoute.isPageRoute: ${newRoute is PageRoute}");
        }*/

        if (newRoute is PageRoute)
            _sendScreenView(newRoute);
    }

    @override
    void didPop(Route<dynamic> route, Route<dynamic> previousRoute)
    {
        super.didPop(route, previousRoute);

        /*if (previousRoute != null)
        {
            print("didPop: previousRoute:             ${nameExtractor(previousRoute.settings)}");
            print("didPop: previousRoute.runtimeType: ${previousRoute.runtimeType}");
            print("didPop: previousRoute.isPageRoute: ${previousRoute is PageRoute}");
        }

        if (route != null)
        {
            print("didPop: route:             ${nameExtractor(route.settings)}");
            print("didPop: route.runtimeType: ${route.runtimeType}");
            print("didPop: route.isPageRoute: ${route is PageRoute}");
        }*/

        if (previousRoute is PageRoute && route is PageRoute)
            _sendScreenView(previousRoute);
    }

    void _sendScreenView(PageRoute<dynamic> route)
    {
        final String screenName = nameExtractor(route.settings);

        //print("AnalyticsNavigatorObserver._sendScreenView: $screenName");

        if (screenName != null)
            GetIt.instance.get<IAnalyticsService>().setCurrentScreen(screenName);
    }
}
