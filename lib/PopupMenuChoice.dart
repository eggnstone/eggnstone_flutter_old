import 'package:flutter/widgets.dart';

class PopupMenuChoice
{
    final String key;
    final IconData iconData;
    final Object value;

    PopupMenuChoice(this.key, this.iconData, this.value);
}
