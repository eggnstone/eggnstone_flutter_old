import 'package:eggnstone_flutter/tools/Time.dart';
import 'package:flutter/material.dart';

class TimeConverter
{
    static toTimeOfDay(Time time)
    {
        return TimeOfDay(hour: time.hours, minute: time.minutes);
    }

    static Time toTime(TimeOfDay tod)
    {
        return Time(tod.hour, tod.minute);
    }
}
