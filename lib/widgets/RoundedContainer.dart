import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RoundedContainer extends StatelessWidget
{
    final double radius;
    final Color backgroundColor;
    final Widget child;

    const RoundedContainer({
        Key key,
        @required this.radius,
        this.backgroundColor,
        @required this.child
    })
        : super(key: key);

    @override
    Widget build(BuildContext context)
    {
        ThemeData theme = Theme.of(context);

        return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(radius)),
                border: Border.all(color: theme.dividerColor),
                color: backgroundColor
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(radius)),
                child: child
            )
        );
    }
}
