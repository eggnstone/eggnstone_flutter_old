import 'package:eggnstone_flutter/painters/RingPainter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CircularProgressWidget extends StatelessWidget
{
    final double size;
    final int numerator;
    final int denominator;
    final double ringWidth;
    final Color backgroundColor;
    final Color completeColor;
    final Color incompleteColor;
    final TextStyle textStylePercentage;
    final TextStyle textStyleFraction;

    final double _percentage100;

    CircularProgressWidget({
        this.size,
        this.numerator,
        this.denominator,
        this.ringWidth,
        this.backgroundColor,
        this.completeColor,
        this.incompleteColor,
        this.textStylePercentage,
        this.textStyleFraction
    })
        : _percentage100 = denominator == 0 ? 0 : numerator * 100.0 / denominator;

    @override
    Widget build(BuildContext context)
    {
        return Stack(
            alignment: Alignment.center,
            children: <Widget>[
                Container(
                    width: size,
                    height: size,
                    child: CustomPaint(
                        painter: RingPainter(
                            numerator: numerator,
                            denominator: denominator,
                            size: size,
                            ringWidth: ringWidth,
                            backgroundColor: backgroundColor,
                            completeColor: completeColor,
                            incompleteColor: incompleteColor
                        )
                    )
                ),
                Center(
                    child: Column(
                        children: <Widget>[
                            Text(_percentage100.toStringAsFixed(0) + '%', style: textStylePercentage),
                            Text('$numerator / $denominator', style: textStyleFraction)
                        ]
                    )
                )
            ]
        );
    }
}
