import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BorderedContainer extends Container
{
    final Color color;
    final double borderWidth;

    BorderedContainer({
        Key key,
        Alignment alignment,
        EdgeInsetsGeometry padding,
        Color color,
        Decoration decoration,
        Decoration foregroundDecoration,
        this.borderWidth = 1.0,
        double width,
        double height,
        BoxConstraints constraints,
        EdgeInsetsGeometry margin,
        Matrix4 transform,
        Widget child,
    })
        : this.color = color ?? Colors.black,
            super(
            key: key,
            alignment: alignment,
            padding: padding,
            color: color ?? Colors.black,
            decoration: decoration,
            foregroundDecoration: foregroundDecoration,
            width: width,
            height: height,
            constraints: constraints,
            margin: margin,
            transform: transform,
            child: child);

    @override
    Widget build(BuildContext context)
    {
        return Container(
            decoration: BoxDecoration(border: Border.all(color: color, width: borderWidth)),
            key: key,
            alignment: alignment,
            padding: padding,
            //color: color,
            foregroundDecoration: foregroundDecoration,
            //width: width,
            //height: height,
            constraints: constraints,
            margin: margin,
            transform: transform,
            child: child
        );
    }
}
