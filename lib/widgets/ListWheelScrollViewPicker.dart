import 'dart:math';

import 'package:eggnstone_flutter/widgets/CenteredText.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class ListWheelScrollViewPicker extends StatefulWidget
{
    final double height;
    final double itemScaleFactor;
    final int initialValue;
    final List<String> items;
    final String label;
    final Color backgroundColor;
    final ValueChanged<int> onChanged;

    ListWheelScrollViewPicker({
        this.height,
        this.itemScaleFactor,
        this.initialValue,
        this.items,
        this.label = '',
        @required this.backgroundColor,
        this.onChanged
    });

    @override
    _ListWheelScrollViewPickerState createState()
    => new _ListWheelScrollViewPickerState();
}

class _ListWheelScrollViewPickerState extends State<ListWheelScrollViewPicker>
{
    static const HORIZONTAL_PADDING = 8.0;
    static const Color DIVIDER_COLOR = Colors.grey;

    ScrollController _scrollController;
    double _maxItemWidth;
    double _maxItemHeight;
    double _labelWidth;
    double _minWidth;
    double _actualWidth;
    int _currentIndex;

    @override
    Widget build(BuildContext context)
    {
        ThemeData themeData = Theme.of(context);
        TextStyle textStyle = themeData.textTheme.body1;

        int initialValue = min(widget.items.length - 1, max(0, widget.initialValue));
        _currentIndex = initialValue;

        _maxItemWidth = 0.0;
        _maxItemHeight = 0.0;
        widget.items.forEach((String s)
        {
            TextSpan ts1 = TextSpan(text: s, style: textStyle);
            TextPainter tp1 = TextPainter(text: ts1, textScaleFactor: widget.itemScaleFactor, textDirection: TextDirection.ltr);
            tp1.layout();
            if (tp1.width > _maxItemWidth)
                _maxItemWidth = tp1.width;
            if (tp1.height > _maxItemHeight)
                _maxItemHeight = tp1.height;
        });

        TextSpan ts2 = TextSpan(text: widget.label, style: textStyle);
        TextPainter tp2 = TextPainter(text: ts2, textDirection: TextDirection.ltr);
        tp2.layout();
        _labelWidth = tp2.width;

        _minWidth = HORIZONTAL_PADDING + _maxItemWidth + HORIZONTAL_PADDING + _labelWidth + HORIZONTAL_PADDING;

        List<Widget> children = [];
        widget.items.forEach((String text)
        {
            children.add(
                Container(
                    margin: EdgeInsets.only(left: HORIZONTAL_PADDING, right: HORIZONTAL_PADDING + _labelWidth + HORIZONTAL_PADDING),
                    width: _maxItemWidth,
                    child: CenteredText(
                        text,
                        textStyle: textStyle,
                        textScaleFactor: widget.itemScaleFactor)
                )
            );
        });

        return LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints)
            {
                _actualWidth = (constraints.maxWidth != double.infinity) ? constraints.maxWidth : _minWidth;
                _scrollController = ScrollController(initialScrollOffset: _currentIndex * _maxItemHeight);

                return Stack(
                    children: <Widget>[
                        Container(
                            width: _actualWidth,
                            height: widget.height,
                            child: NotificationListener(
                                onNotification: _onNotification,
                                child: ListWheelScrollView(
                                    key: Key('ListWheelScrollView_' + DateTime.now().toIso8601String()), // to force redraw after orientation change
                                    controller: _scrollController,
                                    itemExtent: _maxItemHeight,
                                    children: children
                                )
                            )
                        ),
                        IgnorePointer(
                            child: Container(
                                width: _actualWidth,
                                height: widget.height,
                                decoration: ListWheelScrollViewPickerDecoration(_maxItemHeight, widget.backgroundColor, DIVIDER_COLOR, widget.label, textStyle, HORIZONTAL_PADDING),
                            )
                        )
                    ]
                );
            });
    }

    bool _onNotification(Notification notification)
    {
        // TODO: fix warning
        if (notification is UserScrollNotification &&
            notification.direction == ScrollDirection.idle &&
            _scrollController.position.activity is! HoldScrollActivity) // is! ?
        {
            _currentIndex = (notification.metrics.pixels / _maxItemHeight).round();
            widget.onChanged(_currentIndex);
            _scrollController.animateTo(_currentIndex * _maxItemHeight, duration: Duration(seconds: 1), curve: ElasticOutCurve());
            return true;
        }

        return false;
    }
}

class ListWheelScrollViewPickerDecoration extends Decoration
{
    final double itemHeight;
    final Color backgroundColor;
    final Color dividerColor;
    final String label;
    final TextStyle textStyle;
    final double paddingRight;

    ListWheelScrollViewPickerDecoration(this.itemHeight, this.backgroundColor, this.dividerColor, this.label, this.textStyle, this.paddingRight);

    @override
    BoxPainter createBoxPainter([onChanged])
    => ListWheelScrollViewPickerBoxPainter(itemHeight, backgroundColor, dividerColor, label, textStyle, paddingRight);
}

class ListWheelScrollViewPickerBoxPainter extends BoxPainter
{
    final double itemHeight;
    final Color backgroundColor;
    final Color dividerColor;
    final String label;
    final TextStyle textStyle;
    final double paddingRight;

    ListWheelScrollViewPickerBoxPainter(this.itemHeight, this.backgroundColor, this.dividerColor, this.label, this.textStyle, this.paddingRight);

    @override
    void paint(Canvas canvas, Offset offset, ImageConfiguration configuration)
    {
        double left = offset.dx;
        double top = offset.dy;
        double width = configuration.size.width;
        double height = configuration.size.height;

        Color transparentColor = backgroundColor.withOpacity(0.0);

        double fadeHeight = 0.5 * height - 0.5 * itemHeight;
        Rect topFadeRect = Rect.fromLTWH(left, top, width, fadeHeight);
        Gradient topFadeGradient = LinearGradient(begin: FractionalOffset.topCenter, end: FractionalOffset.bottomCenter, colors: [backgroundColor, transparentColor]);
        Shader topFadeShader = topFadeGradient.createShader(topFadeRect);
        Rect bottomFadeRect = Rect.fromLTWH(left, top + height - fadeHeight, width, fadeHeight);
        Gradient bottomFadeGradient = LinearGradient(begin: FractionalOffset.topCenter, end: FractionalOffset.bottomCenter, colors: [transparentColor, backgroundColor]);
        Shader bottomFadeShader = bottomFadeGradient.createShader(bottomFadeRect);

        Paint dividerPaint = Paint()
            ..strokeWidth = 1.0
            ..isAntiAlias = true
            ..color = dividerColor;
        Paint topFadePaint = Paint()
            ..shader = topFadeShader;
        Paint bottomFadePaint = Paint()
            ..shader = bottomFadeShader;

        double middleY = top + 0.5 * height;
        double topDividerY = middleY - 0.5 * itemHeight;
        double bottomDividerY = middleY + 0.5 * itemHeight;

        canvas.drawRect(topFadeRect, topFadePaint);
        canvas.drawRect(bottomFadeRect, bottomFadePaint);
        canvas.drawLine(Offset(left, topDividerY), Offset(left + width, topDividerY), dividerPaint);
        canvas.drawLine(Offset(left, bottomDividerY), Offset(left + width, bottomDividerY), dividerPaint);

        TextSpan ts = TextSpan(text: label, style: textStyle);
        TextPainter tp = TextPainter(text: ts, textDirection: TextDirection.ltr);
        tp.layout();
        double textX = left + width - tp.width - paddingRight;
        double textY = middleY - 0.5 * tp.height;
        tp.paint(canvas, Offset(textX, textY));
    }
}
