import 'package:eggnstone_flutter/painters/CenteredTextPainter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

@Deprecated('Use CenteredText')
class UncutCenteredText extends CenteredText
{
    UncutCenteredText({String text, Color color, double fontSize})
        : super(text, textStyle: TextStyle(fontSize: fontSize, color: color));
}

class CenteredText extends StatelessWidget
{
    final String text;
    final TextStyle textStyle;
    final double textScaleFactor;

    CenteredText(this.text, {@required this.textStyle, this.textScaleFactor = 1.0});

    @override
    Widget build(BuildContext context)
    {
        return LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints)
            {
                TextSpan ts = TextSpan(text: text, style: textStyle);
                TextPainter tp = TextPainter(text: ts, textScaleFactor: textScaleFactor, textDirection: TextDirection.ltr);
                tp.layout();

                double actualWidth = (constraints.maxWidth != double.infinity) ? constraints.maxWidth : 0;
                double actualHeight = tp.height;

                return Container(
                    width: actualWidth,
                    height: actualHeight,
                    child: CustomPaint(
                        painter: CenteredTextPainter(textPainter: tp)
                    )
                );
            }
        );
    }
}
