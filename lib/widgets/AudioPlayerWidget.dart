/*
import 'dart:math';
import 'dart:ui';

import 'package:audioplayers/audioplayers.dart';
import 'package:eggnstone_flutter/loading/indicator/ball_spin_fade_loader_indicator.dart';
import 'package:eggnstone_flutter/loading/loading.dart';
import 'package:eggnstone_flutter/tools/FormatTools.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

typedef void AudioDurationCallback(Duration position);
typedef void AudioPositionCallback(Duration position);
typedef void AudioErrorCallback(String message, StackTrace stackTrace);

class AudioPlayerWidget extends StatefulWidget
{
    final String audioUrl;
    final bool isLocal;
    final bool shouldAutoPlay;
    final double height;
    final Widget backgroundWidget;
    final Color activeColor;
    final Color inactiveColor;
    final Duration startPosition;
    final AudioDurationCallback onDuration;
    final AudioPositionCallback onPosition;
    final VoidCallback onCompletion;
    final AudioErrorCallback onError;

    const AudioPlayerWidget({
        this.audioUrl,
        this.isLocal = false,
        this.shouldAutoPlay = false,
        this.height,
        this.backgroundWidget,
        this.activeColor,
        this.inactiveColor,
        this.startPosition,
        this.onDuration,
        this.onPosition,
        this.onCompletion,
        this.onError
    });

    @override
    _AudioPlayerWidgetState createState()
    => _AudioPlayerWidgetState();

    static void setLogEnabled(bool b)
    {
        AudioPlayer.logEnabled = b;
    }
}

class _AudioPlayerWidgetState extends State<AudioPlayerWidget>
{
    AudioPlayer _audioPlayer;
    bool _isLoaded = false;
    bool _isChangingState = false;
    bool _isSeeking = false;
    double _sliderValue = 0.0;
    String _positionText = '0:00';
    String _sliderLabelText = '0:00';
    String _durationText = '0:00';
    Duration _position = Duration();
    Duration _duration = Duration(minutes: 1);

    @override
    void initState()
    {
        super.initState();
        if (widget.audioUrl != null)
            _initAudioPlayer(widget.audioUrl, widget.isLocal);
    }

    @override
    void didUpdateWidget(AudioPlayerWidget oldWidget)
    {
        super.didUpdateWidget(oldWidget);
        if (oldWidget.audioUrl != widget.audioUrl && widget.audioUrl != null)
            _initAudioPlayer(widget.audioUrl, widget.isLocal);
    }

    void _initAudioPlayer(String url, bool isLocal)
    {
        try
        {
            _audioPlayer = AudioPlayer();

            _audioPlayer.onDurationChanged.listen((Duration value)
            {
                if (mounted == false)
                {
                    print('AudioPlayer.onDurationChanged: not mounted!');
                    return;
                }

                setState(()
                {
                    _isLoaded = true;
                });

                if (_duration == value)
                    return;

                //print('AudioPlayer.onDurationChanged: $value');
                setState(()
                {
                    _duration = value;
                    _durationText = FormatTools.formatMinutesSecondsFromDuration(_duration);
                });

                widget.onDuration(_duration);
            });

            _audioPlayer.onAudioPositionChanged.listen((Duration value)
            {
                if (mounted == false)
                {
                    print('AudioPlayer.onAudioPositionChanged: not mounted!');
                    return;
                }

                //print('AudioPlayer.onAudioPositionChanged: $value');
                setState(()
                {
                    _position = value;
                    _positionText = FormatTools.formatMinutesSecondsFromDuration(_position);

                    if (_isSeeking == false)
                    {
                        _sliderValue = _position.inSeconds.toDouble();
                    }
                });

                widget.onPosition(_position);
            });

            _audioPlayer.onPlayerCompletion.listen((_)
            {
                if (mounted == false)
                {
                    print('AudioPlayer.onPlayerCompletion: not mounted!');
                    return;
                }

                //print('AudioPlayer.onPlayerCompletion');
                setState(()
                {});

                if (widget.onCompletion != null)
                    widget.onCompletion();
            });

            _audioPlayer.setUrl(url, isLocal: isLocal).then((int errorCode)
            async
            {
                if (errorCode == 1)
                {
                    //print('Success: _audioPlayer.setUrl()');
                    if (widget.startPosition != null)
                        await _audioPlayer.seek(widget.startPosition);

                    if (widget.shouldAutoPlay)
                        _onTogglePlayPause();
                }
                else
                    _reportError('Error loading audio: ErrorCode $errorCode', null);
            });
        }
        catch (exception, stackTrace)
        {
            _reportError('Error loading audio: $exception', stackTrace);
        }
    }

    @override
    void dispose()
    {
        super.dispose();

        if (_audioPlayer != null)
        {
            _audioPlayer.stop().then((int errorCode)
            {
                if (errorCode == 1)
                {
                    //print('Success: _audioPlayer.stop()');
                }
                else
                    _reportError('Error stopping audio: ErrorCode $errorCode', null);

                _audioPlayer.dispose();
            });
        }
    }

    @override
    Widget build(BuildContext context)
    {
        IconData playPauseIconData = _audioPlayer == null ? Icons.cloud_queue : _audioPlayer.state == AudioPlayerState.PLAYING ? Icons.pause : Icons.play_arrow;
        Icon playPauseIcon = Icon(playPauseIconData);

        TextTheme textTheme = Theme
            .of(context)
            .textTheme;

        final TextStyle lengthStyle = textTheme.subtitle.copyWith(color: Colors.white);

        Widget positionWidget = Container(
            color: Colors.black.withOpacity(0.5),
            padding: const EdgeInsets.all(4),
            child: Text(
                _positionText,
                style: lengthStyle
            )
        );

        Widget durationWidget = Container(
            color: Colors.black.withOpacity(0.5),
            padding: const EdgeInsets.all(4),
            child: Text(
                _durationText,
                style: lengthStyle
            )
        );

        Widget controlsRow = _isLoaded
            ? Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
                Material(
                    type: MaterialType.circle,
                    clipBehavior: Clip.hardEdge,
                    color: Colors.white54,
                    child: InkWell(
                        child: IconButton(
                            iconSize: 48,
                            icon: Icon(Icons.fast_rewind),
                            onPressed: _isChangingState ? null : _onRewind
                        )
                    )
                ),
                Material(
                    type: MaterialType.circle,
                    clipBehavior: Clip.hardEdge,
                    color: Colors.white54,
                    child: InkWell(
                        child: IconButton(
                            iconSize: 48,
                            icon: playPauseIcon,
                            onPressed: _isChangingState ? null : _onTogglePlayPause
                        )
                    )
                ),
                Material(
                    type: MaterialType.circle,
                    clipBehavior: Clip.hardEdge,
                    color: Colors.white54,
                    child: InkWell(
                        child: IconButton(
                            iconSize: 48,
                            icon: Icon(Icons.fast_forward),
                            onPressed: _isChangingState ? null : _onForward
                        )
                    )
                )
            ]
        )
            : Container();

        Widget player = Container(
            height: widget.height,
            child: Stack(
                children: <Widget>[
                    widget.backgroundWidget,
                    Padding(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                                Expanded(child: Container()),
                                controlsRow,
                                Expanded(child: Container()),
                                Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                        Expanded(
                                            child: Slider(
                                                min: 0.0,
                                                max: (_duration.inMilliseconds / 1000.0),
                                                value: _sliderValue,
                                                label: _sliderLabelText,
                                                activeColor: widget.activeColor,
                                                inactiveColor: widget.inactiveColor,
                                                divisions: _duration.inSeconds,
                                                onChangeStart: (double value)
                                                {
                                                    //print('onChangeStart: $value');
                                                    _isSeeking = true;
                                                },
                                                onChangeEnd: (double value)
                                                {
                                                    //print('onChangeEnd: $value');
                                                    _isSeeking = false;
                                                    _audioPlayer.seek(Duration(milliseconds: (value * 1000.0).round()));
                                                    // TODO: changingstate
                                                },
                                                onChanged: (double value)
                                                {
                                                    //print('onChanged: $value');
                                                    setState(()
                                                    {
                                                        _sliderValue = value;
                                                        _sliderLabelText = FormatTools.formatMinutesSecondsFromSeconds(_sliderValue.round());
                                                    });
                                                }
                                            )
                                        )
                                    ]
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(left: 16, right: 16, bottom: 8),
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                            positionWidget,
                                            Expanded(child: Container()),
                                            durationWidget
                                        ]
                                    )
                                )
                            ]
                        )
                    )
                ]
            )
        );

        if (_isLoaded == false)
            player = Stack(
                children: <Widget>[
                    player,
                    Container(
                        height: widget.height,
                        color: Colors.black.withOpacity(0.5),
                        child: Center(
                            child: Loading(
                                indicator: BallSpinFadeLoaderIndicator(),
                                size: 64.0
                            )
                        )
                    )
                ]
            );

        return player;
    }

    void _onTogglePlayPause()
    {
        if (_isChangingState)
            return;

        if (_audioPlayer.state == AudioPlayerState.PLAYING)
        {
            setState(()
            {
                _isChangingState = true;
            });

            _audioPlayer.pause().then((int errorCode)
            {
                if (errorCode == 1)
                {
                    //print('Success: _audioPlayer.pause()');
                }
                else
                    _reportError('Error pausing audio: ErrorCode $errorCode', null);

                setState(()
                {
                    _isChangingState = false;
                });
            });
        }
        else
        {
            setState(()
            {
                _isChangingState = true;
            });

            _audioPlayer.resume().then((int errorCode)
            {
                if (errorCode == 1)
                {
                    //print('Success: _audioPlayer.resume()');
                }
                else
                    _reportError('Error resuming audio: ErrorCode $errorCode', null);

                setState(()
                {
                    _isChangingState = false;
                });
            });
        }
    }

    void _onRewind()
    {
        if (_isChangingState)
            return;

        //print('_audioPlayer.state: ${_audioPlayer.state}');

        setState(()
        {
            _isChangingState = true;
        });

        //print('_position: $_position');
        int newPositionInMilliseconds = max(0, _position.inMilliseconds - 10 * 1000);
        Duration newPosition = Duration(milliseconds: newPositionInMilliseconds);
        _audioPlayer.seek(newPosition).then((int errorCode)
        {
            if (errorCode == 1)
            {
                //print('Success in onRewind: _audioPlayer.seek()');
            }
            else
                _reportError('Error seeking audio in onRewind: ErrorCode $errorCode', null);

            setState(()
            {
                _isChangingState = false;
            });
        });
    }

    void _onForward()
    {
        if (_isChangingState)
            return;

        //print('_audioPlayer.state: ${_audioPlayer.state}');

        setState(()
        {
            _isChangingState = true;
        });

        //print('_position: $_position');
        int newPositionInMilliseconds = min(_duration.inMilliseconds, _position.inMilliseconds + 10 * 1000);
        Duration newPosition = Duration(milliseconds: newPositionInMilliseconds);
        _audioPlayer.seek(newPosition).then((int errorCode)
        {
            if (errorCode == 1)
            {
                //print('Success in onForward: _audioPlayer.seek()');
            }
            else
                _reportError('Error seeking audio in onForward: ErrorCode $errorCode', null);

            setState(()
            {
                _isChangingState = false;
            });
        });
    }

    void _reportError(String message, StackTrace stackTrace)
    {
        if (widget.onError != null)
            widget.onError(message, stackTrace);
    }
}
*/
