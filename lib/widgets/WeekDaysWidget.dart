import 'package:flutter/material.dart';

typedef void ActiveDaysCallback(List<int> activeDays);

class WeekDaysWidget extends StatefulWidget
{
    // TODO: this should probably be somewhere else
    //static const int FIRST_DAY_OF_WEEK_SYSTEM = -1;
    static const int FIRST_DAY_OF_WEEK_MONDAY = 0;
    static const int FIRST_DAY_OF_WEEK_SATURDAY = 5;
    static const int FIRST_DAY_OF_WEEK_SUNDAY = 6;

    final int actualFirstDayOfWeek;
    final List<int> initialValue;
    final ActiveDaysCallback onChanged;

    WeekDaysWidget({
        Key key,
        this.actualFirstDayOfWeek,
        this.initialValue,
        this.onChanged
    })
        : super(key: key);

    @override
    _WeekDaysWidgetState createState()
    => _WeekDaysWidgetState();
}

class _WeekDaysWidgetState extends State<WeekDaysWidget>
{
    List<int> _activeDays;

    _WeekDaysWidgetState()
    {
        print('_WeekDaysWidgetState.CTOR');
    }

    @override
    void initState()
    {
        print('_WeekDaysWidgetState.initState');
        super.initState();
        _activeDays = [];
        _activeDays.addAll(widget.initialValue);
    }

    @override
    Widget build(BuildContext context)
    {
        print('_WeekDaysWidgetState.build');

        List<Widget> labels = [];
        List<Widget> checkboxes = [];

        final List<String> dayNames = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];

        int offset = WeekDaysWidget.FIRST_DAY_OF_WEEK_MONDAY;
        if (widget.actualFirstDayOfWeek == WeekDaysWidget.FIRST_DAY_OF_WEEK_SATURDAY)
            offset = WeekDaysWidget.FIRST_DAY_OF_WEEK_SATURDAY;
        else if (widget.actualFirstDayOfWeek == WeekDaysWidget.FIRST_DAY_OF_WEEK_SUNDAY)
            offset = WeekDaysWidget.FIRST_DAY_OF_WEEK_SUNDAY;

        for (int i = 0; i < 7; i++)
        {
            labels.add(Center(child: Text(dayNames[(i + offset) % 7])));
            checkboxes.add(_createCheckbox((i + offset) % 7));
        }

        return Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Table(
                children: [
                    TableRow(children: labels),
                    TableRow(children: checkboxes)
                ]
            )
        );
    }

    Widget _createCheckbox(int i)
    {
        return Checkbox(
            value: _activeDays.contains(i),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onChanged: (bool b)
            {
                if (b)
                {
                    if (_activeDays.contains(i) == false)
                        setState(()
                        {
                            _activeDays.add(i);
                            if (widget.onChanged != null)
                                widget.onChanged(_activeDays);
                        });
                }
                else
                {
                    if (_activeDays.contains(i))
                        setState(()
                        {
                            _activeDays.remove(i);
                            if (widget.onChanged != null)
                                widget.onChanged(_activeDays);
                        });
                }
            }
        );
    }
}
