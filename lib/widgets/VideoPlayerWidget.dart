import 'dart:async';
import 'dart:io';

import 'package:eggnstone_flutter/tools/Observable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:video_player/video_player.dart';

typedef void VideoDurationCallback(Duration position);
typedef void VideoPositionCallback(Duration position);
typedef void VideoErrorCallback(String message, StackTrace stackTrace);

class VideoPlayerWidget extends StatefulWidget
{
    final String videoUrl;
    final File videoFile;
    final Observable<File> observableVideoFile;
    final double height;
    final Observable<double> observableAspectRatio;
    final bool shouldLoop;
    final bool shouldAutoPlay;
    final bool showLoadingText;
    final String loadingText;
    final Color color;
    final VideoDurationCallback onDuration;
    final VideoPositionCallback onPosition;
    final VideoErrorCallback onError;

    const VideoPlayerWidget({
        Key key,
        this.videoUrl,
        this.videoFile,
        this.observableVideoFile,
        @required this.height,
        this.observableAspectRatio,
        this.shouldAutoPlay = false,
        this.shouldLoop = false,
        this.showLoadingText = true,
        this.loadingText,
        this.color,
        this.onDuration,
        this.onPosition,
        this.onError
    })
        : super(key: key);

    @override
    _VideoPlayerWidgetState createState()
    => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget>
{
    VideoPlayerController _videoController;
    bool _controlsVisible = true;
    Timer _timer;
    String _noVideoText;
    Duration _lastDuration;
    Duration _lastPosition;

    @override
    void initState()
    {
        super.initState();

        if (widget.showLoadingText)
            _noVideoText = widget.loadingText == null ? 'Loading video ...' : widget.loadingText;
        else
            _noVideoText = '';

        try
        {
            if (widget.observableVideoFile == null)
            {
                if (widget.videoUrl != null)
                    _videoController = VideoPlayerController.network(widget.videoUrl);
                else if (widget.videoFile != null)
                    _videoController = VideoPlayerController.file(widget.videoFile);

                if (_videoController == null)
                {
                    _reportError('Neither observableVideoFile nor videoUrl nor videoFile specified.', null);
                }
                else
                {
                    _videoController.addListener(_onVideoControllerListener);
                    _videoController.initialize().then(_onVideoControllerInitialized, onError: _onVideoControllerInitializationError);
                }
            }
            else
            {
                widget.observableVideoFile.addListener(()
                {
                    if (widget.observableVideoFile.value != null)
                    {
                        _videoController = VideoPlayerController.file(widget.observableVideoFile.value);
                        _videoController.addListener(_onVideoControllerListener);
                        _videoController.initialize().then(_onVideoControllerInitialized, onError: _onVideoControllerInitializationError);
                    }
                }, triggerImmediatelyIfNotNull: true);
            }

            if (widget.observableAspectRatio != null)
                widget.observableAspectRatio.addListener(()
                {
                    if (widget.observableAspectRatio.value != null)
                    {
                        //print('Received new aspect ratio: ${widget.observableAspectRatio.value}');
                        if (mounted)
                            setState(()
                            {});
                    }
                }, triggerImmediatelyIfNotNull: true);
        }
        catch (exception, stackTrace)
        {
            _reportError('Error loading video (1): "$exception"', stackTrace);
        }
    }

    @override
    void dispose()
    {
        super.dispose();

        if (_videoController != null)
        {
            _videoController.removeListener(_onVideoControllerListener);
            _videoController.dispose();
        }
    }

    @override
    Widget build(BuildContext context)
    {
        if (_videoController == null || _videoController.value == null || _videoController.value.initialized == false)
            return Container(
                height: widget.height,
                padding: const EdgeInsets.all(8),
                child: Center(
                    child: Text(_noVideoText)
                )
            );

        double calculatedWidthViaAspectRation;
        if (_videoController.value.aspectRatio > 0.0)
        {
            calculatedWidthViaAspectRation = _videoController.value.aspectRatio * widget.height;
            //print('VideoPlayerWidget: Using aspect ratio from VideoController: Ratio: ${_videoController.value.aspectRatio} Width: $calculatedWidthViaAspectRation');
        }
        else if (widget.observableAspectRatio != null && widget.observableAspectRatio.value != null)
        {
            calculatedWidthViaAspectRation = widget.observableAspectRatio.value * widget.height;
            //print('VideoPlayerWidget: Using supplied aspect ratio via widget: Ratio: ${widget.observableAspectRatio.value} Width: $calculatedWidthViaAspectRation');
        }
        else
        {
            //print('VideoPlayerWidget: No aspect ratio found');
        }

        IconData playPauseIconData = _videoController.value.isPlaying ? Icons.pause : Icons.play_arrow;
        Widget videoControls = GestureDetector(
            child: AnimatedOpacity(
                opacity: _controlsVisible ? 1.0 : 0.0,
                duration: Duration(milliseconds: 500),
                child: Container(
                    color: Colors.black.withOpacity(0.5),
                    child: GestureDetector(
                        child: Center(
                            child: Icon(playPauseIconData, color: Colors.white, size: 64)
                        ),
                        onTap: _onTapPlayPause
                    )
                )
            ),
            onTap: _onTapVideo
        );

        return Container(
            height: widget.height,
            width: calculatedWidthViaAspectRation,
            child: Stack(
                alignment: Alignment.center,
                children: [
                    VideoPlayer(_videoController),
                    videoControls
                ]
            )
        );
    }

    void _onTapVideo()
    {
        if (mounted)
            setState(()
            {
                if (_videoController.value.isPlaying)
                {
                    if (_controlsVisible)
                    {
                        _controlsVisible = false;
                    }
                    else
                    {
                        _controlsVisible = true;
                        _timer = Timer(Duration(milliseconds: 1500), ()
                        {
                            if (mounted)
                                setState(()
                                {
                                    _controlsVisible = false;
                                });
                        });
                    }
                }
                else
                {
                    _controlsVisible = !_controlsVisible;
                }
            });
    }

    void _onTapPlayPause()
    {
        if (_videoController.value.initialized == false)
            return;

        if (_timer != null)
            _timer.cancel();

        if (mounted)
            setState(()
            {
                if (_videoController.value.isPlaying)
                {
                    if (_controlsVisible)
                    {
                        _videoController.pause();
                    }
                    else
                    {
                        _controlsVisible = true;
                        _timer = Timer(Duration(milliseconds: 1500), ()
                        {
                            if (mounted)
                                setState(()
                                {
                                    _controlsVisible = false;
                                });
                        });
                    }
                }
                else
                {
                    if (_controlsVisible)
                    {
                        _videoController.play();
                        _timer = Timer(Duration(milliseconds: 500), ()
                        {
                            if (mounted)
                                setState(()
                                {
                                    _controlsVisible = false;
                                });
                        });
                    }
                    else
                    {
                        _controlsVisible = true;
                        _timer = Timer(Duration(milliseconds: 1500), ()
                        {
                            if (mounted)
                                setState(()
                                {
                                    _controlsVisible = false;
                                });
                        });
                    }
                }
            });
    }

    void _onVideoControllerListener()
    {
        if (_videoController.value == null)
            return;

        if (_videoController.value.hasError)
            _reportError('Error loading video (2): "${_videoController.value.errorDescription}"', null);

        //print('IsBuffering: ' + _videoController.value.isBuffering.toString());
        //print('IsPlaying: ' + _videoController.value.isPlaying.toString());
        //print('Duration: ' + _videoController.value.duration.toString());
        //print('Size: ${_videoController.value.size}');
        //print('Position: ' + _videoController.value.position.toString());

        if (_videoController.value.duration != _lastDuration)
            if (widget.onDuration != null)
                widget.onDuration(_videoController.value.duration);

        if (_videoController.value.position != _lastPosition)
            if (widget.onPosition != null)
                widget.onPosition(_videoController.value.position);

        if (_videoController.value.hasError)
            _reportError('Error loading video (3): "${_videoController.value.errorDescription}"', null);

        _lastDuration = _videoController.value.duration;
        _lastPosition = _videoController.value.position;
    }

    void _reportError(String message, StackTrace stackTrace)
    {
        if (mounted)
            setState(()
            {
                _noVideoText = message;
            });

        if (widget.onError != null)
            widget.onError(message, stackTrace);
    }

    FutureOr _onVideoControllerInitialized(void value)
    {
        try
        {
            if (widget.shouldLoop)
                _videoController.setLooping(true);

            if (widget.shouldAutoPlay)
            {
                _onTapPlayPause();
            }
            else
            {
                // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
                if (mounted)
                    setState(()
                    {});
            }
        }
        catch (exception, stackTrace)
        {
            _reportError('Error loading video (4): "$exception"', stackTrace);
        }
    }

    void _onVideoControllerInitializationError(exception, stackTrace)
    {
        _reportError('Error loading video (5): "$exception"', stackTrace);
    }
}
