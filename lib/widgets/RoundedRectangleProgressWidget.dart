import 'package:eggnstone_flutter/painters/RectanglePainter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedRectangleProgressWidget extends StatelessWidget
{
    final int numerator;
    final int denominator;
    final Color backgroundColor;
    final Color borderColor;
    final Color completeColor;
    final bool showText;

    final Paint _completePaint;

    RoundedRectangleProgressWidget({
        this.numerator,
        this.denominator,
        this.backgroundColor,
        this.borderColor,
        this.completeColor,
        this.showText
    })
        : _completePaint = Paint()
    {
        _completePaint.color = completeColor;
        _completePaint.style = PaintingStyle.stroke;
    }

    @override
    Widget build(BuildContext context)
    {
        TextTheme textTheme = Theme
            .of(context)
            .textTheme;

        return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                border: Border.all(color: borderColor)
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: Stack(
                    fit: StackFit.passthrough,
                    children: <Widget>[
                        CustomPaint(
                            painter: RectanglePainter(
                                numerator: numerator,
                                denominator: denominator,
                                backgroundColor: backgroundColor,
                                completeColor: completeColor
                            )
                        ),
                        showText ? Text('$numerator / $denominator', style: textTheme.caption, textAlign: TextAlign.center) : Container(height: 16)
                    ]
                )
            )
        );
    }
}
