import 'package:flutter/material.dart';
import 'package:flutter_reorderable_list/flutter_reorderable_list.dart';

typedef Widget CreateChildCallback(dynamic item);

abstract class ResortableList<T>
{
    int get length;

    T operator [](int index);

    int indexWhere(Function f);

    void removeAt(int index);

    void insert(int index, T item);
}

class EasyReorderableList<ResortableList> extends StatefulWidget
{
    final ResortableList list;
    final Color backgroundColor;
    final CreateChildCallback createChildCallback;
    final ReorderCompleteCallback reorderCompleteCallback;

    EasyReorderableList({
        this.list,
        this.backgroundColor,
        this.createChildCallback,
        this.reorderCompleteCallback
    });

    @override
    _EasyReorderableListState createState()
    => _EasyReorderableListState();
}

class _EasyReorderableListState extends State<EasyReorderableList>
{
    @override
    Widget build(BuildContext context)
    {
        return OldReorderableList(
            onReorder: _onReorder,
            onReorderDone: widget.reorderCompleteCallback,
            child: ListView.builder(
                itemCount: widget.list.length,
                itemBuilder: (BuildContext context, int index)
                {
                    return ReorderableItem(
                        key: Key(widget.list[index].hashCode.toString()),
                        childBuilder: (BuildContext context, ReorderableItemState state)
                        {
                            return DelayedReorderableListener(
                                child: ReorderableListener(
                                    child: Container(
                                        color: widget.backgroundColor,
                                        child: widget.createChildCallback(widget.list[index])
                                    )
                                )
                            );
                        }
                    );
                }
            )
        );
    }

    int _indexOfKey(Key key)
    {
        return widget.list.indexWhere((Object o)
        => Key(o.hashCode.toString()) == key);
    }

    bool _onReorder(Key key, Key newKey)
    {
        int draggingIndex = _indexOfKey(key);
        int newPositionIndex = _indexOfKey(newKey);

        final draggedItem = widget.list[draggingIndex];
        setState(()
        {
            //debugPrint('Reordering $key -> $newKey');
            widget.list.removeAt(draggingIndex);
            widget.list.insert(newPositionIndex, draggedItem);
        });

        return true;
    }
}
