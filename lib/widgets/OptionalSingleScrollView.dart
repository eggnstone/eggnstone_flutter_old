/*
class OptionalScrollView extends StatefulWidget
{
    final Widget child;

    OptionalScrollView({this.child})
    {
        if (child.key == null)
            throw Exception('OptionalScrollView child needs a key');
    }

    @override
    _OptionalScrollViewState createState()
    => _OptionalScrollViewState();
}

class _OptionalScrollViewState extends State<OptionalScrollView>
{
    var _containerKey = GlobalKey();

    Orientation _lastOrientation;
    Key _lastKey;

    double _maxHeight;
    double _halfPadding = 0.0;

    @override
    void initState()
    {
        super.initState();
        WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    }

    @override
    Widget build(BuildContext context)
    {
        return OrientationBuilder(
            builder: (BuildContext context, Orientation orientation)
            {
                //print('OrientationBuilder.build   orientation=$orientation');

                if ((_lastOrientation != null && _lastOrientation != orientation) ||
                    (_lastKey != null && _lastKey != widget.child.key))
                {
                    //print('##### Reset');
                    //_maxHeight = null;
                    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
                }

                _lastOrientation = orientation;
                _lastKey = widget.child.key;

                return LayoutBuilder(
                    builder: (BuildContext context, BoxConstraints constraints)
                    {
                        //print('LayoutBuilder.build   constraints.maxHeight=${constraints.maxHeight}');

                        _maxHeight = constraints.maxHeight;
                        return SingleChildScrollView(
                            child: Padding(
                                padding: EdgeInsets.only(top: _halfPadding, bottom: _halfPadding),
                                child: Container(
                                    key: _containerKey,
                                    child: widget.child
                                )
                            )
                        );
                    }
                );
            }
        );
    }

    void _afterLayout(_)
    {
        //print('_afterLayout');

        if (_containerKey.currentContext != null)
        {
            RenderBox containerRenderBox = _containerKey.currentContext.findRenderObject();

            //print('_afterLayout containerRenderBox.size.height=${containerRenderBox.size.height}');

            double newHalfPadding = (_maxHeight - containerRenderBox.size.height) / 2;
            if (newHalfPadding < 0.0)
                newHalfPadding = 0.0;

            newHalfPadding = 0.0;

            //print('newHalfPadding=$newHalfPadding');

            if (newHalfPadding != _halfPadding)
            {
                setState(()
                {
                    _halfPadding = newHalfPadding;
                });
            }
        }
    }
}
*/
