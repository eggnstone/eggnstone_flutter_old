import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CenteredSingleScrollView extends StatefulWidget
{
    final Widget child;

    CenteredSingleScrollView({this.child})
    {
        if (child.key == null)
            throw Exception('CenteredSingleScrollView child needs a key');
    }

    @override
    State<StatefulWidget> createState()
    => _CenteredSingleScrollViewState();
}

class _CenteredSingleScrollViewState extends State<CenteredSingleScrollView>
{
    var _containerKey = GlobalKey();

    Orientation _lastOrientation;
    Key _lastChildKey;
    double _lastMaxHeight;

    double _maxHeight;
    double _halfPadding = 0.0;

    @override
    void initState()
    {
        super.initState();
        //print('_CenteredSingleScrollViewState.initState   Scheduling addPostFrameCallback');
        WidgetsBinding.instance.addPostFrameCallback(_afterLayout1);
    }

    @override
    Widget build(BuildContext context)
    {
        //print('_CenteredSingleScrollViewState.build   ${widget.child.key}');

        return OrientationBuilder(
            builder: (BuildContext context, Orientation orientation)
            {
                //print('_CenteredSingleScrollViewState.build.OrientationBuilder.build   orientation=$orientation');

                if ((_lastOrientation != null && _lastOrientation != orientation) ||
                    (_lastChildKey != null && _lastChildKey != widget.child.key))
                {
                    //print('_CenteredSingleScrollViewState.build.OrientationBuilder.build   Scheduling addPostFrameCallback after orientation or child has changed.');
                    WidgetsBinding.instance.addPostFrameCallback(_afterLayout2);
                }

                _lastOrientation = orientation;
                _lastChildKey = widget.child.key;

                return LayoutBuilder(
                    builder: (BuildContext context, BoxConstraints constraints)
                    {
                        //print('_CenteredSingleScrollViewState.build.OrientationBuilder.build.LayoutBuilder.build   constraints.maxHeight=${constraints.maxHeight}');

                        // TODO: write test for this
                        _maxHeight = constraints.maxHeight;
                        if (_lastMaxHeight != _maxHeight)
                        {
                            //print('_CenteredSingleScrollViewState.build.OrientationBuilder.build.LayoutBuilder.build   Scheduling addPostFrameCallback after maxHeight has changed.');
                            WidgetsBinding.instance.addPostFrameCallback(_afterLayout3);
                        }

                        return SingleChildScrollView(
                            child: Padding(
                                padding: EdgeInsets.only(top: _halfPadding, bottom: _halfPadding),
                                child: Container(
                                    key: _containerKey,
                                    child: widget.child
                                )
                            )
                        );
                    }
                );
            }
        );
    }

    void _afterLayout1(_)
    {
        //print('_CenteredSingleScrollViewState._afterLayout1');
        _afterLayout(null);
    }

    void _afterLayout2(_)
    {
        //print('_CenteredSingleScrollViewState._afterLayout2');
        _afterLayout(null);
    }

    void _afterLayout3(_)
    {
        //print('_CenteredSingleScrollViewState._afterLayout3');
        _afterLayout(null);
    }

    void _afterLayout(_)
    {
        //print('_CenteredSingleScrollViewState._afterLayout');

        if (_containerKey.currentContext != null)
        {
            RenderBox containerRenderBox = _containerKey.currentContext.findRenderObject();

            //print('_CenteredSingleScrollViewState._afterLayout   containerRenderBox.size.height=${containerRenderBox.size.height}');

            double newHalfPadding = (_maxHeight - containerRenderBox.size.height) / 2;
            if (newHalfPadding < 0.0)
                newHalfPadding = 0.0;

            //print('_CenteredSingleScrollViewState._afterLayout   newHalfPadding=$newHalfPadding');

            if (newHalfPadding != _halfPadding)
            {
                setState(()
                {
                    _halfPadding = newHalfPadding;
                });
            }
        }
    }
}
