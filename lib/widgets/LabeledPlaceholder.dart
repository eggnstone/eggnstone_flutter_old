import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LabeledPlaceholder extends Placeholder
{
    final String name;
    final double fallbackWidth;
    final double fallbackHeight;

    LabeledPlaceholder(this.name, { this.fallbackWidth = 400.0, this.fallbackHeight = 400.0 });

    @override
    Widget build(BuildContext context)
    {
        return Stack(
            alignment: Alignment.center,
            children: <Widget>[
                Placeholder(
                    fallbackWidth: fallbackWidth,
                    fallbackHeight: fallbackHeight,
                ),
                Container(
                    decoration: BoxDecoration(color: Colors.white),
                    child: Text(name)
                )
            ]
        );
    }
}
