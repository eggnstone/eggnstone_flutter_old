import 'package:eggnstone_flutter/tools/Time.dart';
import 'package:eggnstone_flutter/widgets/ListWheelScrollViewPicker.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';

class TimePicker extends StatefulWidget
{
    final double height;
    final Time time;
    final String hourLabel;
    final String minuteLabel;
    final Color backgroundColor;
    final bool use24HourFormat;
    final ValueChanged<Time> onChanged;

    TimePicker({
        this.height = 125.0,
        @required this.time, this.hourLabel,
        this.minuteLabel,
        @required this.backgroundColor,
        this.use24HourFormat,
        @required this.onChanged
    });

    @override
    TimePickerState createState()
    => new TimePickerState();
}

class TimePickerState extends State<TimePicker>
{
    List<String> _hourItems;
    List<String> _minuteItems;
    List<String> _periodItems;

    ListWheelScrollViewPicker _hourPicker;
    ListWheelScrollViewPicker _minutePicker;
    ListWheelScrollViewPicker _periodPicker;
    int _currentHourIndex;
    int _currentMinuteIndex;
    int _currentPeriodIndex;
    bool _use24HourFormat;
    bool _initialized = false;

    @override
    void didChangeDependencies()
    {
        super.didChangeDependencies();

        _init(Localizations.localeOf(context));
    }

    void _init(Locale locale)
    {
        _hourItems = [];
        _minuteItems = [];
        _periodItems = [];

        // UGLY !!!
        if (widget.use24HourFormat == null)
            _use24HourFormat = DateFormat.jm(locale.languageCode).format(DateTime(0, 0, 0, 23, 59, 59)) == '23:59';
        else
            _use24HourFormat = widget.use24HourFormat;

        /* print('DateFormat.jm() ' + DateFormat.jm().format(DateTime(0, 0, 0, 23, 59, 59)));
        print('DateFormat.jm(locale.languageCode) ' + DateFormat.jm(locale.languageCode).format(DateTime(0, 0, 0, 23, 59, 59)));
        print('DateTimetoString ' + DateTime(0, 0, 0, 23, 59, 59).toString());
        print('DateTimetoIso8601String ' + DateTime(0, 0, 0, 23, 59, 59).toIso8601String());*/

        for (int i = 0; i < 60; i++)
            _minuteItems.add(i.toString());

        _currentMinuteIndex = widget.time.minutes;

        if (_use24HourFormat)
        {
            for (int i = 0; i < (_use24HourFormat ? 24 : 12); i++)
                _hourItems.add(sprintf('%02i', [i]));

            _currentHourIndex = widget.time.hours;
            _currentPeriodIndex = null;
        }
        else
        {
            for (int i = 1; i <= 12; i++)
                _hourItems.add(i.toString());

            _periodItems.add('AM');
            _periodItems.add('PM');

            _currentHourIndex = (widget.time.hours + 11) % 12;
            _currentPeriodIndex = widget.time.hours < 12 ? 0 : 1;
        }

        _hourPicker = ListWheelScrollViewPicker(
            height: widget.height,
            itemScaleFactor: 1.5,
            initialValue: _currentHourIndex,
            items: _hourItems,
            label: widget.hourLabel,
            backgroundColor: widget.backgroundColor,
            onChanged: (int index)
            {
                _currentHourIndex = index;
                _onChanged();
            }
        );

        _minutePicker = ListWheelScrollViewPicker(
            height: widget.height,
            itemScaleFactor: 1.5,
            initialValue: _currentMinuteIndex,
            items: _minuteItems,
            label: widget.minuteLabel,
            backgroundColor: widget.backgroundColor,
            onChanged: (int index)
            {
                _currentMinuteIndex = index;
                _onChanged();
            }
        );

        if (_use24HourFormat == false)
            _periodPicker = ListWheelScrollViewPicker(
                height: widget.height,
                itemScaleFactor: 1.5,
                initialValue: _currentPeriodIndex,
                items: _periodItems,
                backgroundColor: widget.backgroundColor,
                onChanged: (int index)
                {
                    _currentPeriodIndex = index;
                    _onChanged();
                }
            );

        _initialized = true;
    }

    @override
    Widget build(BuildContext context)
    {
        if (_initialized == false)
            return Container();

        List<Widget> children = [
            Expanded(child: _hourPicker),
            Expanded(child: _minutePicker)
        ];

        if (_use24HourFormat == false)
            children.add(Expanded(child: _periodPicker));

        return Container(
            height: widget.height,
            child: Row(children: children)
        );
    }

    void _onChanged()
    {
        if (_use24HourFormat)
            widget.onChanged(Time(_currentHourIndex, _currentMinuteIndex));
        else
            widget.onChanged(Time((_currentHourIndex + 1) % 12 + _currentPeriodIndex * 12, _currentMinuteIndex));
    }
}
