import 'package:flutter/widgets.dart';

class ObservableWidget extends StatefulWidget
{
    final Widget child;
    final Function onInitState;
    final Function onDispose;

    ObservableWidget({
        this.child,
        this.onInitState,
        this.onDispose
    });

    @override
    _ObservableWidgetState createState()
    => _ObservableWidgetState();
}

class _ObservableWidgetState extends State<ObservableWidget>
{
    @override
    void initState()
    {
        super.initState();
        widget.onInitState();
    }

    @override
    void dispose()
    {
        widget.onDispose();
        super.dispose();
    }

    @override
    Widget build(BuildContext context)
    {
        return widget.child;
    }
}
