import 'package:flutter/painting.dart';

class DayShapeBorder extends ShapeBorder
{
    final bool isFirstDay;
    final double triangleSize;

    DayShapeBorder(this.isFirstDay, this.triangleSize);

    @override
    EdgeInsetsGeometry get dimensions
    {
        return null;
    }

    @override
    Path getInnerPath(Rect rect, {TextDirection textDirection})
    {
        return null;
    }

    @override
    Path getOuterPath(Rect rect, {TextDirection textDirection})
    {
        Path path = Path();

        List<Offset> points;

        if (isFirstDay)
            points = [
                Offset(rect.left + triangleSize, rect.top),
                Offset(rect.right, rect.top),
                Offset(rect.right, rect.bottom),
                Offset(rect.left, rect.bottom),
                Offset(rect.left, rect.top + triangleSize),
            ];
        else
            points = [
                Offset(rect.left, rect.top),
                Offset(rect.right, rect.top),
                Offset(rect.right, rect.bottom - triangleSize),
                Offset(rect.right - triangleSize, rect.bottom),
                Offset(rect.left, rect.bottom),
            ];

        path.addPolygon(points, true);

        return path;
    }

    @override
    void paint(Canvas canvas, Rect rect, {TextDirection textDirection})
    {
    }

    @override
    ShapeBorder scale(double t)
    {
        return null;
    }
}
