import 'package:eggnstone_flutter/loading/loading.dart';
import 'package:flutter/widgets.dart';

abstract class Indicator
{
    LoadingState loadingState;
    List<AnimationController> animationControllers;
    bool isDisposed = false;

    paint(Canvas canvas, Paint paint, Size size);

    List<AnimationController> animation();

    void postInvalidate()
    {
        // TODO: fix warning
        loadingState.setState(()
        {});
    }

    void start()
    {
        animationControllers = animation();
        if (animationControllers != null)
        {
            startAnimations(animationControllers);
        }
    }

    void dispose()
    {
        if (animationControllers != null)
        {
            for (var i = 0; i < animationControllers.length; i++)
            {
                animationControllers[i].dispose();
            }
        }

        isDisposed = true;
    }

    void startAnimations(List<AnimationController> controllers)
    {
        for (var i = 0; i < controllers.length; i++)
        {
            startAnimation(controllers[i]);
        }
    }

    void startAnimation(AnimationController controller)
    {
        controller.repeat();
    }
}
