import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:eggnstone_flutter/services/logger/LoggerMixin.dart';

// TODO: is this a service?
/// Requires [ILoggerService]
class DeviceInfo
    with LoggerMixin
{
    String type;
    String info;
    String id;

    DeviceInfo._internal()
    {
        assert(logger != null, 'Unable to find via GetIt: Logger');
    }

    /// Requires [ILoggerService]
    static Future<DeviceInfo> create()
    async
    {
        var instance = DeviceInfo._internal();
        await instance._init();
        return Future.value(instance);
    }

    Future<void> _init()
    async
    {
        try
        {
            if (Platform.isAndroid)
            {
                type = 'Android';
                info = '';
                AndroidDeviceInfo androidInfo = await DeviceInfoPlugin().androidInfo;
                id = androidInfo.androidId;
                Map<String, String> map = _readAndroidBuildData(androidInfo);
                try
                {
                    /*
                    info += 'version.sdkInt=' + map['version.sdkInt'] + '\t';
                    info += 'version.release=' + map['version.release'] + '\t';
                    info += 'board=' + map['board'] + '\t';
                    info += 'brand=' + map['brand'] + '\t';
                    info += 'device=' + map['device'] + '\t';
                    info += 'hardware=' + map['hardware'] + '\t';
                    info += 'manufacturer=' + map['manufacturer'] + '\t';
                    info += 'model=' + map['model'] + '\t';
                    info += 'product=' + map['product'] + '\t';
                    */
                    map.forEach((k, v)
                    {
                        info += k + '=' + v + '\t';
                    });
                }
                catch (e)
                {
                    info += '(Exception: $e)';
                }
            }
            else if (Platform.isIOS)
            {
                type = 'iOS';
                info = '';
                IosDeviceInfo iosInfo = await DeviceInfoPlugin().iosInfo;
                id = iosInfo.identifierForVendor;
                Map<String, String> map = _readIosDeviceInfo(iosInfo);
                try
                {
                    map.forEach((k, v)
                    {
                        info += k + '=' + v + '\t';
                    });
                }
                catch (e)
                {
                    info += '(Exception: $e)';
                }
            }
            else
            {
                type = '???';
                info = '???';
                id = '???';
            }
        }
        catch (e)
        {
            logger.logInfo('Exception: ' + e);
        }
    }

    Map<String, String> _readAndroidBuildData(AndroidDeviceInfo build)
    {
        return <String, String>{
            //'version.securityPatch': build.version.securityPatch,
            'version.sdkInt': build.version.sdkInt.toString(),
            'version.release': build.version.release,
            //'version.previewSdkInt': build.version.previewSdkInt.toString(),
            //'version.incremental': build.version.incremental,
            //'version.codename': build.version.codename,
            //'version.baseOS': build.version.baseOS,
            'board': build.board,
            //'bootloader': build.bootloader,
            'brand': build.brand,
            'device': build.device,
            //'display': build.display,
            //'fingerprint': build.fingerprint,
            'hardware': build.hardware,
            //'host': build.host,
            //'id': build.id,
            'manufacturer': build.manufacturer,
            'model': build.model,
            'product': build.product,
            //'supported32BitAbis': build.supported32BitAbis.toString(),
            //'supported64BitAbis': build.supported64BitAbis.toString(),
            //'supportedAbis': build.supportedAbis.toString(),
            //'tags': build.tags,
            //'type': build.type,
            //'isPhysicalDevice': build.isPhysicalDevice.toString(),
        };
    }

    Map<String, String> _readIosDeviceInfo(IosDeviceInfo data)
    {
        return <String, String>{
            //'name': data.name,
            'systemName': data.systemName,
            'systemVersion': data.systemVersion,
            'model': data.model,
            //'localizedModel': data.localizedModel,
            //'identifierForVendor': data.identifierForVendor,
            //'isPhysicalDevice': data.isPhysicalDevice.toString(),
            //'utsname.sysname:': data.utsname.sysname,
            //'utsname.nodename:': data.utsname.nodename,
            //'utsname.release:': data.utsname.release,
            //'utsname.version:': data.utsname.version,
            'utsname.machine:': data.utsname.machine,
        };
    }
}
