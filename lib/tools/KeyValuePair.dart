class KeyValuePair
{
    final String key;
    final Object value;

    KeyValuePair(this.key, this.value);
}
