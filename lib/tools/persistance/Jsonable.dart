abstract class Jsonable
{
    Jsonable();

    Jsonable.fromJson(String s)
    {
        init(s);
    }

    String toJson();

    void init(String jsonText);
}
