import 'package:eggnstone_flutter/tools/persistance/Jsonable.dart';

abstract class Persistable extends Jsonable
{
    String get filename;

    String get externalStorageSubDirName;
}