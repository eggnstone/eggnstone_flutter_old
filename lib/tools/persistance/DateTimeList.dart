import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:eggnstone_flutter/tools/persistance/Jsonable.dart';

class DateTimeList extends Jsonable
{
    List<DateTime> items = [];

    DateTimeList.fromJson(String s) : super.fromJson(s);

    @override
    String toJson()
    {
        List<String> datesAsText = [];

        for (DateTime d in items)
        {
            String dateAsText = d.toIso8601String();
            datesAsText.add(dateAsText);
        }

        return jsonEncode(datesAsText);
    }

    @override
    void init(String jsonText)
    {
        items.clear();

        List list = jsonDecode(jsonText);
        for (String s in list)
            items.add(DateTime.parse(s));
    }

    bool contains(DateTime d)
    => items.contains(d);

    void add(DateTime d)
    {
        items.add(d);
    }

    void remove(DateTime d)
    {
        items.remove(d);
    }

    @override
    bool operator ==(Object other)
    {
        if (other == null)
            return false;

        if (other is DateTimeList)
            return ListEquality().equals(items, other.items);
        else
            return false;
    }

    @override
    int get hashCode
    => items.hashCode;
}
