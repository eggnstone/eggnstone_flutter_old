import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:eggnstone_flutter/tools/persistance/Jsonable.dart';

class IntList extends Jsonable
{
    List<int> items = [];

    int get length
    => items.length;

    IntList();

    IntList.fromJson(String s) : super.fromJson(s);

    @override
    String toJson()
    => jsonEncode(items);

    @override
    void init(String jsonText)
    {
        items.clear();

        if (jsonText == null || jsonText.length == 0)
            return;

        List list = jsonDecode(jsonText);
        for (int i in list)
            items.add(i);
    }

    void add(int i)
    {
        items.add(i);
    }

    void insert(int index, int i)
    {
        items.insert(index, i);
    }

    int operator [](int index)
    => items[index];

    void operator []=(int index, int value)
    {
        items[index] = value;
    }

    void remove(int i)
    {
        items.remove(i);
    }

    void removeAt(int index)
    {
        items.removeAt(index);
    }

    void clear()
    {
        items.clear();
    }

    bool contains(int i)
    => items.contains(i);

    IntList clone()
    {
        var newList = IntList();

        newList.items.addAll(items);

        return newList;
    }

    @override
    bool operator ==(Object other)
    {
        if (other == null)
            return false;

        if (other is IntList)
            return ListEquality().equals(items, other.items);
        else
            return false;
    }

    @override
    int get hashCode
    => items.hashCode;
}
