import 'dart:async';
import 'dart:io';

import 'package:eggnstone_flutter/tools/persistance/Persistable.dart';
import 'package:path_provider/path_provider.dart';

class Persisted<T extends Persistable>
{
    Future<String> get _localPath
    async
    {
        Directory directory = await getApplicationDocumentsDirectory();
        return directory.path;
    }

    Future<File> get _localFile
    async
    {
        String path = await _localPath;
        return File('$path/${(this as Persistable).filename}');
    }

    Future<bool> readFromApplicationDocumentsDirectory()
    async
    {
        try
        {
            File file = await _localFile;

            if (await file.exists() == false)
            {
                //print('Persisted.readFromApplicationDocumentsDirectory: file does not exists: ${file.path}');
                return false;
            }

            //print('Reading data from internal file ${file.path} ...');
            String jsonText = await file.readAsString();
            //print('Read data from internal file ${file.path}: ' + jsonText);

            if (jsonText == null || jsonText.length == 0)
                return false;

            //print('Parsing JSON data from ${(this as Persistable).filename} ...');
            (this as Persistable).init(jsonText);
            //print('Parsed JSON data from ${(this as Persistable).filename}.');

            return true;
        }
        catch (e)
        {
            print('Persisted.readFromApplicationDocumentsDirectory: $e');
            return false;
        }
    }

    @Deprecated('Use writeToApplicationDocumentsDirectory insetad')
    Future<void> write(bool b)
    => writeToApplicationDocumentsDirectory();

    Future<void> writeToApplicationDocumentsDirectory()
    async
    {
        //print('Converting data to JSON for ${(this as Persistable).filename} ...');
        String jsonText = (this as Persistable).toJson();
        //print('Converted data to JSON for ${(this as Persistable).filename}: ' + jsonText);

        File file = await _localFile;
        //print('Writing data to internal file ${file.path} ...');
        await file.writeAsString(jsonText, flush: true);
        //print('Written data to internal file ${file.path}.');

        return null;
    }
}
