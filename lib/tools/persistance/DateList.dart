import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:eggnstone_flutter/tools/Date.dart';
import 'package:eggnstone_flutter/tools/persistance/Jsonable.dart';

class DateList extends Jsonable
{
    List<Date> items = [];

    DateList();

    DateList.fromJson(String s) : super.fromJson(s);

    int get length
    => items.length;

    @override
    String toJson()
    => jsonEncode(toStringList());

    List<String> toStringList()
    {
        List<String> list = [];

        for (Date date in items)
            list.add(date.toString());

        return list;
    }

    @override
    void init(String jsonText)
    {
        items.clear();

        if (jsonText == null || jsonText.length == 0)
            return;

        List list = jsonDecode(jsonText);
        for (String s in list)
            items.add(Date.parse(s));
    }

    bool contains(Date d)
    => items.contains(d);

    void add(Date d)
    {
        items.add(d);
    }

    void remove(Date d)
    {
        items.remove(d);
    }

    void clear()
    {
        items.clear();
    }

    @override
    bool operator ==(Object other)
    {
        if (other == null)
            return false;

        if (other is DateList)
            return ListEquality().equals(items, other.items);
        else
            return false;
    }

    @override
    int get hashCode
    => items.hashCode;
}
