import 'dart:ui';

import 'package:eggnstone_flutter/tools/persistance/Jsonable.dart';
import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';

class Time extends Jsonable
    implements Comparable<Time>
{
    int hours;
    int minutes;
    int seconds;

    Time(this.hours, this.minutes, [this.seconds = 0]);

    Time.fromDateTime(DateTime d)
    {
        _set(d);
    }

    Time.now()
    {
        _set(DateTime.now());
    }

    Time.fromJson(String s)
    {
        init(s);
    }

    Time.fromTotalSeconds(int totalSeconds)
    {
        hours = (totalSeconds / 3600).round();
        minutes = (totalSeconds % 3600 / 60).round();
        seconds = totalSeconds % 60;
    }

    @override
    String toJson()
    => toString();

    @override
    void init(String jsonText)
    {
        hours = int.parse(jsonText.substring(0, 2));
        minutes = int.parse(jsonText.substring(2, 4));
        seconds = int.parse(jsonText.substring(4, 6));
    }

    static Time parse(String s)
    {
        return Time(
            int.parse(s.substring(0, 2)),
            int.parse(s.substring(2, 4)),
            int.parse(s.substring(4, 6))
        );
    }

    @override
    String toString()
    {
        return sprintf('%02i%02i%02i', [hours, minutes, seconds]);
    }

    void _set(DateTime d)
    {
        hours = d.hour;
        minutes = d.minute;
        seconds = d.second;
    }

    bool operator ==(Object other)
    {
        if (other == null)
            return null; // ???

        if (other is Time)
            return hours == other.hours && minutes == other.minutes && seconds == other.seconds;
        else
            return false;
    }

    @override
    int get hashCode
    => hours * 24 * 60 + minutes * 60 + seconds;

    @override
    int compareTo(Time other)
    {
        if (hours < other.hours)
            return -1;

        if (hours > other.hours)
            return 1;

        if (minutes < other.minutes)
            return -1;

        if (minutes > other.minutes)
            return 1;

        if (seconds < other.seconds)
            return -1;

        if (seconds > other.seconds)
            return 1;

        return 0;
    }

    DateTime toDateTime()
    => DateTime.utc(0, 0, 0, hours, minutes, seconds);

    int toTotalSeconds()
    => hours * 3600 + minutes * 60 + seconds;

    String toShortString(Locale locale)
    => DateFormat.jm(locale.languageCode).format(toDateTime());

    String to12HoursMinutesString()
    => DateFormat.jm('en_US').format(toDateTime());

    String to24HoursMinutesString()
    => DateFormat.jm('de').format(toDateTime());

    String to24HoursMinutesSecondsString()
    => DateFormat('HH:mm:ss').format(toDateTime());

    Time addSeconds(int i)
    {
        DateTime dt = toDateTime();
        dt = dt.add(Duration(seconds: i));
        Time t = Time.fromDateTime(dt);

        return t;
    }

    Time addMinutes(int i)
    {
        DateTime dt = toDateTime();
        dt = dt.add(Duration(minutes: i));
        Time t = Time.fromDateTime(dt);

        return t;
    }

    Time addHours(int i)
    {
        DateTime dt = toDateTime();
        dt = dt.add(Duration(hours: i));
        Time t = Time.fromDateTime(dt);

        return t;
    }

    Time subtract(Time other)
    {
        DateTime dt = toDateTime();
        DateTime dtOther = other.toDateTime();
        Duration d = dt.difference(dtOther);
        Time t = Time(0, 0).addSeconds(d.inSeconds);

        return t;
    }

    Time clone()
    => Time(hours, minutes, seconds);
}
