import 'dart:ui';

import 'package:eggnstone_flutter/tools/Date.dart';
import 'package:eggnstone_flutter/tools/Time.dart';

class DateAndTime
{
    Date date;
    Time time;

    DateAndTime(int year, int month, int day, int hour, int minute, [int second = 0])
    {
        date = Date(year, month, day);
        time = Time(hour, minute, second);
    }

    DateAndTime.fromDateAndTime(Date d, Time t)
    {
        date = d;
        time = t;
    }

    DateAndTime.fromDateTime(DateTime dt)
    {
        _set(dt);
    }

    DateAndTime.now()
    {
        _set(DateTime.now());
    }

    static DateAndTime parse(String s)
    {
        return DateAndTime(
            int.parse(s.substring(0, 4)),
            int.parse(s.substring(4, 6)),
            int.parse(s.substring(6, 8)),
            int.parse(s.substring(8, 10)),
            int.parse(s.substring(10, 12)),
            int.parse(s.substring(12, 14))
        );
    }

    String toShortString(Locale locale)
    {
        return date.toShortString(locale) + ' ' + time.toShortString(locale);
    }

    @override
    String toString()
    {
        return date.toString() + time.toString();
    }

    void _set(DateTime dt)
    {
        date = Date.fromDateTime(dt);
        time = Time.fromDateTime(dt);
    }
}
