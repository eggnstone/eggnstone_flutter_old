import 'package:sprintf/sprintf.dart';

// TODO: is this a service?
class FormatTools
{
    static String formatMinutesSecondsFromSeconds(int totalSeconds)
    {
        int minutes = (totalSeconds / 60).floor();
        int seconds = totalSeconds % 60;

        return sprintf('%i:%02i', [minutes, seconds]);
    }

    static String formatMinutesSecondsFromDuration(Duration duration)
    {
        int totalSeconds = (duration.inMilliseconds / 1000.0).round();

        return formatMinutesSecondsFromSeconds(totalSeconds);
    }
}
