import 'dart:io';

// TODO: is this a service?
class NetworkTools
{
    // return true if
    // - not a physical device
    // - a Firebase test lab device
    // - a Google PlayStore pre-launch test device
    static bool isTestDevice(String deviceIpText, String host)
    {
        print('isTestDevice: $deviceIpText');

        InternetAddress deviceIp;
        try
        {
            deviceIp = InternetAddress(deviceIpText);
        }
        catch (exception)
        {
            print('isTestDevice: error getting InternetAddress');
            return false;
        }

        print('isTestDevice: $deviceIp');
        if (deviceIp.type != InternetAddressType.IPv4)
        {
            print('isTestDevice: not IP4');
            return false;
        }

        if (host != null)
        {
            if (host.endsWith('.hot.corp.google.com'))
            {
                print('host ends with .hot.corp.google.com');
                return true;
            }
        }

        // Firebase
        // https://firebase.google.com/docs/test-lab/android/overview#and_mobile_advertising
        List<String> cidrAddresses = List();
        // Physical devices
        cidrAddresses.add('108.177.6.0/23');
        // Virtual devices
        cidrAddresses.add('34.68.194.64/29'); // (added 11-2019)
        cidrAddresses.add('34.69.234.64/29/29'); // (added 11-2019)
        cidrAddresses.add('34.73.34.72/29/29'); // (added 11-2019)
        cidrAddresses.add('34.73.178.72/29/29'); // (added 11-2019)
        cidrAddresses.add('35.192.160.56/29');
        cidrAddresses.add('35.196.166.80/29');
        cidrAddresses.add('35.196.169.240/29');
        cidrAddresses.add('35.203.128.0/28');
        cidrAddresses.add('35.234.176.160/28');
        cidrAddresses.add('35.243.2.0/27'); // (added 7-2019)
        cidrAddresses.add('199.192.115.0/30');
        cidrAddresses.add('199.192.115.8/30');
        cidrAddresses.add('199.192.115.16/29');

        for (String cidrAddress in cidrAddresses)
        {
            List<String> parts = cidrAddress.split('/');
            String testLabIpText = parts[0];
            String cidrPrefixText = parts[1];
            int cidrPrefix = int.parse(cidrPrefixText);

            InternetAddress subnetIp = getSubnetIp(cidrPrefix);
            //print(subnetIp);

            String reducedDeviceIp = reduceIp(deviceIp, subnetIp);
            //print(reducedDeviceIp);

            InternetAddress testLabIp = InternetAddress(testLabIpText);
            String reducedTestLabIp = reduceIp(testLabIp, subnetIp);
            //print(reducedTestLabIp);

            if (reducedDeviceIp == reducedTestLabIp)
            {
                print('host is known test host');
                return true;
            }
        }

        return false;
    }

    static InternetAddress getSubnetIp(int cidrPrefix)
    {
        String subnetText = '';
        String x = '';
        for (int i = 1; i <= 32; i++)
        {
            x += (i <= cidrPrefix) ? '1' : '0';

            if (i % 8 == 0 && i > 0)
            {
                subnetText += int.parse(x, radix: 2).toString();
                if (i < 32)
                    subnetText += '.';

                x = '';
            }
        }

        //print(subnetText);
        return InternetAddress(subnetText);
    }

    static String reduceIp(InternetAddress ip, InternetAddress subnetIp)
    {
        String result = '';

        for (int i = 0; i < 4; i++)
        {
            result += (ip.rawAddress[i] & subnetIp.rawAddress[i]).toString();
            if (i < 3)
                result += '.';
        }

        return result;
    }
}
