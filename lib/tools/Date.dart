import 'dart:ui';

import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';

class Date
    implements Comparable<Date>
{
    int year;
    int month;
    int day;

    Date(this.year, this.month, this.day);

    Date.fromDateTime(DateTime d)
    {
        _set(d);
    }

    Date.today()
    {
        _set(DateTime.now());
    }

    /// zero-based weekday, week starts with Monday
    int get weekday
    {
        DateTime dt = toUtcDateTime();

        // DateTime weekday definition:
        // In accordance with ISO 8601
        // a week starts with Monday, which has the value 1.
        int zeroBasedWeekday = dt.weekday - 1;

        return zeroBasedWeekday;
    }

    static Date parse(String s)
    {
        return Date(
            int.parse(s.substring(0, 4)),
            int.parse(s.substring(4, 6)),
            int.parse(s.substring(6, 8))
        );
    }

    @override
    String toString()
    => sprintf('%04i%02i%02i', [year, month, day]);

    void _set(DateTime d)
    {
        year = d.year;
        month = d.month;
        day = d.day;
    }

    bool operator ==(Object other)
    {
        if (other == null)
            return false;

        if (other is Date)
            return year == other.year && month == other.month && day == other.day;
        else
            return false;
    }

    @override
    int get hashCode
    => year * 366 * 31 + month * 31 + day;

    @override
    int compareTo(Date other)
    {
        if (year < other.year)
            return -1;

        if (year > other.year)
            return 1;

        if (month < other.month)
            return -1;

        if (month > other.month)
            return 1;

        if (day < other.day)
            return -1;

        if (day > other.day)
            return 1;

        return 0;
    }

    Date subtractDays(int i)
    {
        DateTime dt = toUtcDateTime();
        dt = dt.subtract(Duration(days: i));
        Date d = Date.fromDateTime(dt);

        return d;
    }

    Date addDays(int i)
    {
        DateTime dt = toUtcDateTime();
        dt = dt.add(Duration(days: i));
        Date d = Date.fromDateTime(dt);

        return d;
    }

    DateTime toUtcDateTime()
    => DateTime.utc(year, month, day);

    int differenceInDays(Date other)
    {
        // When dealing with dates or historic events prefer to use UTC DateTimes,
        // since they are unaffected by daylight-saving changes and are unaffected
        // by the local timezone.
        Duration diff = toUtcDateTime().difference(other.toUtcDateTime());

        return diff.inDays;
    }

    String toShortString(Locale locale)
    => DateFormat.yMd(locale.languageCode).format(toUtcDateTime());

    String toShortTextMonthAndDayString(Locale locale)
    => DateFormat.MMMd(locale.languageCode).format(toUtcDateTime());
}
