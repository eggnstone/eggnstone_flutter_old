import 'package:flutter/widgets.dart';

class Observable<T>
    with ChangeNotifier
{
    final T initialValue;

    T _value;

    Observable({this.initialValue})
    {
        _value = initialValue;
    }

    T get value
    => _value;

    set value(T value)
    {
        if (value != _value)
        {
            _value = value;
            notifyListeners();
        }
    }

    @override
    void addListener(VoidCallback listener,
        {
            triggerImmediately = false,
            triggerImmediatelyIfNotNull = false,
            triggerImmediatelyIfDifferentFromInitialValue = false
        })
    {
        super.addListener(listener);

        if (triggerImmediately)
            listener();
        else if (triggerImmediatelyIfNotNull && _value != null)
            listener();
        else if (triggerImmediatelyIfDifferentFromInitialValue && _value != initialValue)
            listener();
    }
}
