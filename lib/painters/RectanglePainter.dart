import 'package:flutter/widgets.dart';

class RectanglePainter extends CustomPainter
{
    final int numerator;
    final int denominator;
    final Color backgroundColor;
    final Color completeColor;

    final Paint _backgroundPaint;
    final Paint _completePaint;

    RectanglePainter({
        this.numerator,
        this.denominator,
        this.backgroundColor,
        this.completeColor})
        : _backgroundPaint = Paint(),
            _completePaint = Paint()
    {
        _backgroundPaint.color = backgroundColor;
        _completePaint.color = completeColor;
    }

    @override
    void paint(Canvas canvas, Size size)
    {
        double percentage = denominator == 0 ? 0 : numerator / denominator;
        double width = numerator == 0 ? 8 : numerator == denominator ? size.width : 8 + percentage * (size.width - 16);

        canvas.drawRect(Rect.fromLTWH(width, 0, size.width, 16), _backgroundPaint);
        canvas.drawRect(Rect.fromLTWH(0, 0, width, 16), _completePaint);
    }

    @override
    bool shouldRepaint(CustomPainter oldDelegate)
    {
        return true;
    }
}
