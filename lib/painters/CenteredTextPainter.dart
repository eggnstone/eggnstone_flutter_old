import 'package:flutter/widgets.dart';

class CenteredTextPainter extends CustomPainter
{
    final TextPainter textPainter;

    CenteredTextPainter({this.textPainter});

    @override
    void paint(Canvas canvas, Size size)
    {
        textPainter.paint(
            canvas,
            Offset(
                size.width / 2.0 - textPainter.width / 2.0,
                size.height / 2.0 - textPainter.height / 2.0
            )
        );
    }

    @override
    bool shouldRepaint(CenteredTextPainter oldDelegate)
    => false;
}
