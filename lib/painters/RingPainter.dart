import 'package:flutter/widgets.dart';
import 'package:vector_math/vector_math_64.dart' as math;

class RingPainter extends CustomPainter
{
    final int numerator;
    final int denominator;
    final double size;
    final double ringWidth;
    final Color backgroundColor;
    final Color completeColor;
    final Color incompleteColor;

    final double _percentage;
    final double _halfRingWidth;
    final Paint _backgroundPaint;
    final Paint _completePaint;
    final Paint _incompletePaint;
    final Paint _nothingCompletePaint;

    RingPainter({
        this.numerator,
        this.denominator,
        this.size,
        this.ringWidth,
        this.backgroundColor,
        this.completeColor,
        this.incompleteColor})
        : _percentage = denominator == 0 ? 0 : numerator / denominator,
            _halfRingWidth = ringWidth / 2.0,
            _backgroundPaint = Paint(),
            _completePaint = Paint(),
            _incompletePaint = Paint(),
            _nothingCompletePaint = Paint()
    {
        _backgroundPaint.color = backgroundColor;
        _completePaint.color = completeColor;
        _completePaint.style = PaintingStyle.stroke;
        _completePaint.strokeWidth = ringWidth;
        _incompletePaint.color = incompleteColor;
        _incompletePaint.style = PaintingStyle.stroke;
        _incompletePaint.strokeWidth = ringWidth;
        _nothingCompletePaint.color = completeColor;
        _nothingCompletePaint.style = PaintingStyle.fill;
        _nothingCompletePaint.strokeWidth = 1.0;
    }

    @override
    void paint(Canvas canvas, _)
    {
        Offset offset = Offset(size / 2.0, size / 2.0);
        canvas.drawCircle(offset, size / 2.0 - ringWidth, _backgroundPaint);

        Rect rect = Rect.fromLTWH(_halfRingWidth, _halfRingWidth, size - ringWidth, size - ringWidth);
        canvas.drawArc(rect, math.radians(_percentage * 360.0 - 90.0), math.radians(360.0 - _percentage * 360.0), false, _incompletePaint);

        if (numerator == 0)
            canvas.drawCircle(Offset(size / 2.0, _halfRingWidth), _halfRingWidth, _nothingCompletePaint);
        else
            canvas.drawArc(rect, math.radians(-90.0), math.radians(_percentage * 360.0), false, _completePaint);
    }

    @override
    bool shouldRepaint(CustomPainter oldDelegate)
    {
        return true;
    }
}
