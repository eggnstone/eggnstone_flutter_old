import 'package:flutter/widgets.dart';

class ElementTools
{
    static Element getParent(Element child)
    {
        Element parent;
        child.visitAncestorElements((Element element)
        {
            parent = element;
            return false;
        });

        return parent;
    }
}
