
import 'package:eggnstone_flutter/tools/DartTools.dart';
import 'package:flutter_test/flutter_test.dart';

void main()
{
    test('isDebug should be true because tests are run in debug', ()
    {
        expect(DartTools.isDebugBuild(), true);
    });

    test('isRelease should be false because tests are run in debug', ()
    {
        expect(DartTools.isReleaseBuild(), false);
    });
}
