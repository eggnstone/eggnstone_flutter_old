import 'package:eggnstone_flutter/tools/Date.dart';
import 'package:flutter_test/flutter_test.dart';

void main()
{
    test('differenceInDays', ()
    {
        Date d1 = Date(2019, 6, 1);
        Date d2 = Date(2019, 12, 1);

        int differenceInDays = d2.differenceInDays(d1);
        expect(differenceInDays, 183);
    });
}
