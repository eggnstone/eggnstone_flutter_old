import 'package:eggnstone_flutter/ElementTools.dart';
import 'package:eggnstone_flutter/widgets/CenteredSingleScrollView.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

void main()
{
    const double PORTRAIT_WIDTH = 400.0;
    const double PORTRAIT_HEIGHT = 800.0;
    const double LANDSCAPE_WIDTH = PORTRAIT_HEIGHT;
    const double LANDSCAPE_HEIGHT = PORTRAIT_WIDTH;

    // fits in all orientations
    const double SMALL_CONTAINER_HEIGHT = 200.0;

    // fits portrait, but not landscape
    const double CONTAINER_HEIGHT = 600.0;

    // does not fit in any orientation
    const double BIG_CONTAINER_HEIGHT = 1000.0;

    final TestWidgetsFlutterBinding _binding = TestWidgetsFlutterBinding.ensureInitialized();

    testWidgets('Container smaller than portrait height should have filling padding', (WidgetTester tester)
    async
    {
        var childKey = GlobalKey();
        Widget child = Container(key: childKey, height: CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should have filling padding at the top');
        expect(edgeInsets.bottom, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should have filling padding at the bottom');
    });

    testWidgets('Container bigger than portrait height should have no padding', (WidgetTester tester)
    async
    {
        var childKey = GlobalKey();
        Widget child = Container(key: childKey, height: BIG_CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, 0, reason: 'Should have no padding at the top');
        expect(edgeInsets.bottom, 0, reason: 'Should have no padding at the bottom');
    });

    testWidgets('Container smaller than portrait height but bigger than landscape height should have no padding after rotating to landscape', (WidgetTester tester)
    async
    {
        var childKey = GlobalKey();
        Widget child = Container(key: childKey, height: CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child));
        await tester.pumpAndSettle();

        print('Rotating to landscape ...');

        await _binding.setSurfaceSize(Size(LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, 0, reason: 'Should have no padding at the top after rotation');
        expect(edgeInsets.bottom, 0, reason: 'Should have no padding at the bottom after rotation');
    });

    testWidgets('Container smaller than portrait height but bigger than landscape height should have filling padding after rotating to portrait', (WidgetTester tester)
    async
    {
        var childKey = GlobalKey();
        Widget child = Container(key: childKey, height: CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(LANDSCAPE_WIDTH, LANDSCAPE_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child));
        await tester.pumpAndSettle();

        print('Rotating to landscape ...');

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should have filling padding at the top after rotation');
        expect(edgeInsets.bottom, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should have filling padding at the bottom after rotation');
    });

    testWidgets('Increasing but still fitting container should keep filling padding', (WidgetTester tester)
    async
    {
        var childKey1 = GlobalKey();
        var childKey2 = GlobalKey();

        Widget child1 = Container(key: childKey1, height: SMALL_CONTAINER_HEIGHT);
        Widget child2 = Container(key: childKey2, height: CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child1));
        await tester.pumpAndSettle();

        print('Increasing container height ...');

        await tester.pumpWidget(CenteredSingleScrollView(child: child2));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey2);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should still have filling padding at the top after increasing');
        expect(edgeInsets.bottom, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should still have filling padding at the bottom after increasing');
    });

    testWidgets('Increasing container should switch from filling padding to no padding', (WidgetTester tester)
    async
    {
        var childKey1 = GlobalKey();
        var childKey2 = GlobalKey();

        Widget child1 = Container(key: childKey1, height: CONTAINER_HEIGHT);
        Widget child2 = Container(key: childKey2, height: BIG_CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child1));
        await tester.pumpAndSettle();

        print('Increasing container height ...');

        await tester.pumpWidget(CenteredSingleScrollView(child: child2));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey2);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, 0, reason: 'Should have no padding at the top after increasing');
        expect(edgeInsets.bottom, 0, reason: 'Should have no padding at the bottom after increasing');
    });

    testWidgets('Decreasing but still fitting container should keep filling padding', (WidgetTester tester)
    async
    {
        var childKey1 = GlobalKey();
        var childKey2 = GlobalKey();

        Widget child1 = Container(key: childKey1, height: CONTAINER_HEIGHT);
        Widget child2 = Container(key: childKey2, height: SMALL_CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child1));
        await tester.pumpAndSettle();

        print('Decreasing container height ...');

        await tester.pumpWidget(CenteredSingleScrollView(child: child2));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey2);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, (PORTRAIT_HEIGHT - SMALL_CONTAINER_HEIGHT) / 2, reason: 'Should still have filling padding at the top after decreasing');
        expect(edgeInsets.bottom, (PORTRAIT_HEIGHT - SMALL_CONTAINER_HEIGHT) / 2, reason: 'Should still have filling padding at the bottom after decreasing');
    });

    testWidgets('Decreasing container should switch from no padding to filling padding', (WidgetTester tester)
    async
    {
        var childKey1 = GlobalKey();
        var childKey2 = GlobalKey();

        Widget child1 = Container(key: childKey1, height: BIG_CONTAINER_HEIGHT);
        Widget child2 = Container(key: childKey2, height: CONTAINER_HEIGHT);

        await _binding.setSurfaceSize(Size(PORTRAIT_WIDTH, PORTRAIT_HEIGHT));
        await tester.pumpWidget(CenteredSingleScrollView(child: child1));
        await tester.pumpAndSettle();

        print('Decreasing container height ...');

        await tester.pumpWidget(CenteredSingleScrollView(child: child2));
        await tester.pumpAndSettle();

        Finder widgetFinder = find.byKey(childKey2);

        Element childElement = tester.element(widgetFinder);
        Element intermediateElement = ElementTools.getParent(childElement);
        Element parentElement = ElementTools.getParent(intermediateElement);

        Padding padding = parentElement.widget;
        EdgeInsets edgeInsets = padding.padding;
        expect(edgeInsets.top, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should still have filling padding at the top after decreasing');
        expect(edgeInsets.bottom, (PORTRAIT_HEIGHT - CONTAINER_HEIGHT) / 2, reason: 'Should still have filling padding at the bottom after decreasing');
    });
}
